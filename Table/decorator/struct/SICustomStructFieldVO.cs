﻿using UKon.Data.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNen.DT
{
    public class SICustomStructFieldVO : LeafFieldVO, IStructDecorator
    {
        private string m_ownername;
        public SICustomStructFieldVO(string ownername)
        {
            m_ownername = ownername;
        }
        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError(string.Format("Error! field结构name属性不能为空，field name={0}", m_ownername));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(m_name) == false)
            {
                DebugMgr.LogError(string.Format("Error。field结构name属性命名不规范; field name = {0}", m_name));
                return false;
            }

            if (string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError(string.Format("Error! field结构type属性类型不能为空，field name={0}", m_name));
                return false;
            }

            var result = FieldTypeInspector.CheckFieldType(this);
            if (result == false)
            {
                return false;
            }

            return true;
        }
    }
}
