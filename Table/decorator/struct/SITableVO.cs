﻿using UKon.Data.Table;
using System.Collections.Generic;
using System.Xml;

namespace UNen.DT
{
    public class SITableVO : TableVO, IStructDecorator
    {
        protected override void ParseChild(XmlNode node)
        {
            ParseChildren<SIFieldVO, SINodeVO>(node);
        }

        protected bool CheckStructFunc(IStructDecorator vo)
        {
            if (vo == null)
            {
                DebugMgr.LogError("CheckStructFunc 类型转换失败");
                return false;
            }
            return vo.CheckStruct();
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(m_name) == true)
            {
                DebugMgr.LogError(string.Format("Error! table结构name属性不能为空，table={0}", m_name));
                return false;
            }
            if (AttrUtils.CheckNamingSpecification(m_name) == false)
            {
                DebugMgr.LogError(string.Format("Error。table结构name属性命名不规范; tablename = {0}", m_name));
                return false;
            }

            var m_leaves = FindLeaves();
            var primarykeys = m_leaves.FindAll(c => c.IsPrimaryKey == true);
            if (primarykeys.Count > 1)
            {
                DebugMgr.LogError(string.Format("Error! 唯一主键重复; tablename = {0}", m_name));
                return false;
            }
            var unionkeys = m_leaves.FindAll(c => c.IsUnionKey == true);
            if (unionkeys.Count > 2)
            {
                DebugMgr.LogError(string.Format("Error! 联合主键不能超过两个字段; tablename = {0}", m_name));
                return false;
            }

            if (primarykeys.Count > 1 && unionkeys.Count > 1)
            {
                DebugMgr.LogError(string.Format("Error! 唯一主键不能与联合主键同时使用; tablename = {0}", m_name));
                return false;
            }
            if (primarykeys.Count == 0 && unionkeys.Count == 0)
            {
                DebugMgr.LogError(string.Format("Error! 配置表不能没有主键或联合主键; tablename = {0}", m_name));
                return false;
            }

            bool result = true;
            for (int i = 0; i < m_fields.Count; i++)
            {
                result = CheckStructFunc(m_fields[i] as IStructDecorator);
                if (result == false)
                {
                    return false;
                }
            }

            int cnt = ConfigMgr.tableConf.CountVO(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error。表格重复; tablename = {0}", m_name));
                return false;
            }

            return true;
        }
    }
}
