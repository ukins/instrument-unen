﻿using System.Xml;
using UKon.Data;
using UKon.Data.Table;

namespace UNen.DT
{
    public class SIFieldVO : LeafFieldVO, IStructDecorator
    {
        protected SITableVO m_foreigntablevo;
        public SITableVO ForeignTableVO { get { return m_foreigntablevo; } }

        public override void Parse(XmlNode node)
        {
            ParseField(node);
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError(string.Format("Error! field结构name属性不能为空，field name={0}", FullName));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(m_name) == false)
            {
                DebugMgr.LogError(string.Format("Error。field结构name属性命名不规范; field name = {0}", FullName));
                return false;
            }

            if (string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError(string.Format("Error! field结构type属性类型不能为空，field name={0}", FullName));
                return false;
            }

            if (m_type == DTDefine.TUPLE)
            {
                DebugMgr.LogError(string.Format("Error! tuple类型必须包含子节点，field name={0}", FullName));
                return false;
            }
            //if (Type.IsCollection == true && InCollection == true)
            //{
            //    DebugMgr.LogError(string.Format("Error! 容器类型不允许嵌套，field name={0}", FullName));
            //    return false;
            //}

            var result = FieldTypeInspector.CheckFieldType(this);
            if (result == false)
            {
                return false;
            }

            if (m_isprimarykey == true || m_isunionkey == true)
            {
                if ((m_Owner is TableVO) == false)
                {
                    DebugMgr.LogError(string.Format("Error！ 唯一主键不能出现在嵌套字段中。tablename={0}, fieldname = {1}", Root.Name, FullName));
                    return false;
                }

                bool isvaluetype = SysDefine.CheckType(m_type, SysDefine.ProtoTypeConst);
                if (isvaluetype == false)
                {
                    var enumvo = ConfigMgr.tableGenericConf.FindEnum(m_type);
                    if (enumvo == null)
                    {
                        DebugMgr.LogError(string.Format("Error！ 主键类型无效。主键类型必须是值类型或字符串类型。tablename={0}, fieldname = {1}", Root.Name, FullName));
                        return false;
                    }
                }
            }


            if (m_foreigntable != string.Empty)
            {
                m_foreigntablevo = ConfigMgr.tableConf.FindVO(m_foreigntable);
                if (m_foreigntablevo == null)
                {
                    DebugMgr.LogError(string.Format("Error！ 外键无效。 外界表不存在 tablename={0}, fieldname = {1}, type = {2}", Root.Name, FullName, m_type));
                    return false;
                }
                if (Type.FirstElementType != m_foreigntablevo.PrimaryKey.Type.Value)
                {
                    DebugMgr.LogError(string.Format("Error！ 外键无效。类型与关联表主键类型不符 tablename={0}, fieldname = {1}, type = {2} ", Root.Name, FullName, m_type));
                    return false;
                }
            }

            int cnt = m_Owner.CountChildVO(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error！ 字段重复。tablename={0}, fieldname = {1}", Root.Name, FullName));
                return false;
            }
            return true;
        }
    }
}
