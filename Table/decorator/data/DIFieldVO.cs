﻿using UKon.Data;
using UKon.Data.Table;
using System;
using System.Collections.Generic;
using System.Xml;
using UNen.Table;

namespace UNen.DT
{
    public class DIFieldVO : SIFieldVO, IDataDecorator
    {
        public override void Parse(XmlNode node)
        {
            ParseField(node);
        }

        public bool CheckInternalData()
        {
            var m_root = Root as DITableVO;

            List<DiscreteVO> dvos = ConfigMgr.tableConf.FindDiscreteVOByTableName(m_root.Name);
            foreach (var dvo in dvos)
            {
                AbstractDataTable sdt = dvo.GetDataTable();
                for (int i = 0; i < sdt.Rows.Count; ++i)
                {
                    GenericDataRow row = sdt.Rows[i];
                    var leaves = row[FullName];
                    foreach (var item in leaves)
                    {
                        var slice = item.Slice;
                        //foreach (var value in slice)
                        for (int k = 0; k < slice.Count; k++)
                        {
                            string value = slice[k];
                            if (SupportFieldType.ContainComma(value) == true)
                            {
                                DebugMgr.LogError("Error！ 格式错误。不能包含逗号(,)");
                                DebugMgr.LogError(string.Format("\ttablename={0}, fieldname={1}, filepath={2}", m_root.Name, FullName, dvo.Path));
                                return false;
                            }

                            if (DTDefine.CanSwitch2TgtType(value, Type.FirstElementType) == false)
                            {
                                bool flag = false;
                                int enumcnt = ConfigMgr.tableGenericConf.GetEnumCnt(Type.FirstElementType);
                                if (enumcnt > 0)
                                {
                                    var enumvo = ConfigMgr.tableGenericConf.FindEnum(Type.FirstElementType);
                                    var enumfields = enumvo.FindAll();
                                    bool isexist = enumfields.Exists(c => c.Name == value);
                                    if (isexist == true)
                                    {
                                        flag = true;
                                    }
                                    else
                                    {
                                        DebugMgr.LogError(string.Format("\ttablename={0}, fieldname={1}, filepath={2}", m_root.Name, FullName, dvo.Path));
                                        DebugMgr.LogError(string.Format("\tvalue=\'{0}\' 不在枚举范围内", value));
                                        DebugMgr.WriteLine();
                                    }
                                }

                            var structvo = ConfigMgr.tableGenericConf.FindStruct(Type.FirstElementType);
                            if (structvo != null && structvo.Extern == true)
                            {
                                flag = true;
                            }

                            if (flag == false)
                            {
                                DebugMgr.LogError(string.Format("\ttablename={0}, fieldname={1}, filepath={2}", m_root.Name, FullName, dvo.Path));
                                DebugMgr.LogError(string.Format("\tvalue=\'{0}\' 无法转化成【{1}】类型", value, Type.FirstElementType));
                                DebugMgr.WriteLine();
                                return false;
                            }
                        }
                        //if (m_root.Name == "ProtocolTable" && m_name == "Type")
                        //{
                        //    string name = row["Parameter.Name"].Slice[k];
                        //    if (name == string.Empty)
                        //    {
                        //        continue;
                        //    }
                        //    string[] elements = value.Split(':');
                        //    if (elements.Length < 1 || elements.Length > 2)
                        //    {
                        //        DebugMgr.LogError("Error! 类型错误。");
                        //        DebugMgr.LogError(string.Format("tablename={0}, fieldname={1}, filepath={2}", m_root.Name, FullName, dvo.Path));

                            //        return false;
                            //    }
                            //    if (elements.Length == 2)
                            //    {
                            //        if ((elements[1].ToLower() == "list" || elements[1].ToLower() == "array") == false)
                            //        {
                            //            DebugMgr.LogError(string.Format("Error! 类型错误。仅支持list与array"));
                            //            DebugMgr.LogError(string.Format("tablename={0}, fieldname={1}, filepath={2}", m_root.Name, FullName, dvo.Path));
                            //            return false;
                            //        }
                            //    }
                            //    bool isSysType = SysDefine.CheckType(elements[0], SysDefine.ProtoTypeConst);
                            //    if (isSysType == false)
                            //    {
                            //        int cnt = ConfigMgr.protoConf.GetStructCnt(elements[0]);
                            //        if (cnt == 0)
                            //        {
                            //            DebugMgr.LogError("Error! 类型尚未定义。");
                            //            DebugMgr.LogError(string.Format("tablename={0}, fieldname={1}, filepath={2}", m_root.Name, FullName, dvo.Path));
                            //            DebugMgr.LogError(string.Format("value = {0}", value));
                            //            return false;
                            //        }
                            //    }
                            //}


                        }
                    }
                }
            }

            AbstractDataTable dt = m_root.GetDataTable();
            //主键查重
            if (m_isprimarykey == true)
            {
                HashSet<string> m_primaryvalue = new HashSet<string>();
                for (int i = 0; i < dt.Rows.Count; ++i)
                {
                    GenericDataRow row = dt.Rows[i];
                    var leaves = row[FullName];
                    foreach (var item in leaves)
                    {
                        string value = item.Value;
                        if (m_primaryvalue.Contains(value) == true)
                        {
                            DebugMgr.LogError("Error！  主键重复");
                            DebugMgr.LogError(string.Format("\ttablename={0}, fieldname={1}, value={2}", m_root.Name, m_name, value));
                            DebugMgr.WriteLine();
                            return false;
                        }
                        else
                        {
                            m_primaryvalue.Add(value);
                        }
                    }
                }
            }

            return true;
        }

        public bool CheckExternalData()
        {
            var m_root = Root as DITableVO;
            if (m_foreigntablevo != null)
            {
                List<DiscreteVO> dvos = ConfigMgr.tableConf.FindDiscreteVOByTableName(m_root.Name);
                foreach (var dvo in dvos)
                {
                    AbstractDataTable srcdt = dvo.GetDataTable();
                    for (int i = 0; i < srcdt.Rows.Count; ++i)
                    {
                        GenericDataRow row = srcdt.Rows[i];
                        var leaves = row[FullName];
                        foreach (var item in leaves)
                        {
                            var slice = item.Slice;
                            foreach (var value in slice)
                            {

	                            if (string.IsNullOrEmpty(value) == true)
	                            {
	                                continue;
	                            }
                                if (AttrUtils.CheckForeignKeyIsExist(value, m_foreigntablevo as DITableVO) == false)
                                {
                                    DebugMgr.LogError("Error!  外键数据不存在。");
                                    DebugMgr.LogError(string.Format("\ttablename={0},fieldname={1},value={2}", m_root.Name, FullName, value));
                                    DebugMgr.LogError(string.Format("\tfieldpath={0}", dvo.Path));
                                    DebugMgr.WriteLine();
                                    return false;
                                }
                            }
                        }
                    }
                }

            }
            return true;
        }
    }
}
