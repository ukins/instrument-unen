﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Data.Table;

namespace UNen
{
    public class CSVDataTableAgent
    {
        public static bool Parse(string filepath, TableVO tvo, out AbstractDataTable dataTable)
        {
            dataTable = null;
            if (FileUtils.CheckFileIsOpen(filepath) == true)
            {
                DebugMgr.LogError("Error!  文件被其他进程占用。请先关闭其他进程");
                DebugMgr.LogError(string.Format("\ttablename={0},path={1}", tvo.Name, filepath));
                DebugMgr.WriteLine();
                return false;
            }
            if(File.Exists(filepath)==false)
            {
                DebugMgr.LogError("Error!  文件不存在");
                DebugMgr.LogError(string.Format("\ttablename={0},path={1}", tvo.Name, filepath));
                return false;
            }
            //switch (tvo.TableKeyType)
            //{
            //    case ETableKeyType.PrimaryKey:
            //        dataTable = new PrimaryKeyDataTable(tvo.Name);
            //        break;
            //    case ETableKeyType.UnionKey:
            //        dataTable = new UnionKeyDataTable(tvo.Name);
            //        break;
            //    default: return false;
            //}


            //Encoding encoding = FileUtils.GetType(filepath);
            bool result = FileUtils.CheckIsUTF8WithBOM(filepath);
            if (result == false)
            {
                DebugMgr.LogError("Error!  文件编码格式错误！ 仅支持UTF-8 with BOM格式！");
                DebugMgr.LogError(string.Format("\ttablename={0},path={1}", tvo.Name, filepath));
                DebugMgr.WriteLine();
                return false;
            }

            FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs, Encoding.UTF8);
            //StringReader sr = new StringReader(streamreader.ReadToEnd());

            CSVDataTable csvdt = new CSVDataTable(tvo);
            csvdt.Parse(sr.ReadToEnd());
            dataTable = csvdt.DataTable;

            //if (tvo.Name == "FootholdTable")
            //{
            //    //dataTable.Rows
            //    //for (int i = 0; i < dataTable.TotalRowCnt; ++i)
            //    //{
            //    //    var fields = dataTable.Rows.FindLeafFieldsByRowIdx(i);
            //    //    if (fields.Count > 0)
            //    //    {
            //    //        StringBuilder sb = new StringBuilder();
            //    //        sb.AppendFormat("line {0,4}:\t", i);
            //    //        foreach (var item in fields)
            //    //        {
            //    //            sb.Append("[");
            //    //            sb.Append(item.ToString());
            //    //            sb.Append("]");
            //    //            sb.Append("\t");
            //    //        }
            //    DebugMgr.Log(dataTable.Rows.ToCSVString());

            //    //    }
            //    //}
            //}
            ////记录每次读取的一行记录  
            //string strLine = "";

            ////记录每行记录中的各字段内容  
            //string[] row_fields = null;
            //string[] col_fields = null;

            ////int columnCount = 0;
            //var fieldvos = tvo.FindAllFields();

            ////逐行读取CSV中的数据  
            //while ((strLine = sr.ReadLine()) != null)
            //{
            //    if (tvo.Name == "WndTable")
            //    {
            //        DebugMgr.Log(strLine);
            //    }
            //    row_fields = strLine.Split(',');
            //    if (row_fields[0].StartsWith("#") == true)
            //    {
            //        continue;
            //    }
            //    if (row_fields.Length == 1 && fieldvos.Count >= 1)
            //    {
            //        DebugMgr.LogError("Error!  配置表分隔符必须为逗号。");
            //        DebugMgr.LogError(string.Format("\t tablename = {0}, path={1}", tvo.FullName, filepath));
            //        DebugMgr.WriteLine();

            //        sr.Close();
            //        fs.Close();

            //        return false;
            //    }

            //    //生成表头
            //    if (row_fields[0] == "&&")
            //    {
            //        if (col_fields != null)
            //        {
            //            DebugMgr.LogWarning(string.Format("Warning! \"&&\"开头的行表示表头，不应该多于一行。 tablename = {0}", tvo.Name));
            //            continue;
            //        }
            //        col_fields = row_fields;

            //        for (int i = 0; i < fieldvos.Count; i++)
            //        {
            //            GenericDataColumn dc = new GenericDataColumn(fieldvos[i]);
            //            dataTable.Columns.Add(dc);
            //        }

            //        for (int i = 0; i < col_fields.Length; ++i)
            //        {
            //            bool isexist = fieldvos.Exists(c => c.FullName == col_fields[i]);
            //            if (isexist == false)
            //            {
            //                col_fields[i] = string.Empty;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        GenericDataRow dr = dataTable.NewRow();

            //        for (int i = 0; i < col_fields.Length; i++)
            //        {
            //            if (col_fields[i] == string.Empty)
            //            {
            //                continue;
            //            }
            //            for (int j = 0; j < dataTable.Columns.Count; ++j)
            //            {
            //                if (dataTable.Columns[j].ColumnName == col_fields[i])
            //                {
            //                    if (row_fields.Length <= i)
            //                    {
            //                        dr[j].SetValue("");
            //                    }
            //                    else
            //                    {
            //                        dr[j].SetValue(row_fields[i]);
            //                    }
            //                    break;
            //                }
            //            }
            //        }

            //        dataTable.Rows.Add(dr);
            //    }
            //}

            //sr.Close();
            sr.Close();
            fs.Close();

            HashSet<string> hset = new HashSet<string>();
            string columnname = string.Empty;
            if (tvo.PrimaryKey != null)
            {
                columnname = tvo.PrimaryKey.FullName;
                for (int i = 0; i < dataTable.Rows.Count; ++i)
                {
                    string rowvalue = dataTable.Rows[i][columnname][0].ToString();
                    if (hset.Contains(rowvalue) == true)
                    {
                        DebugMgr.LogError(string.Format("Error! 主键重复。tablename={0},fieldname={1},value={2}", tvo.Name, columnname, rowvalue));
                        return false;
                    }
                    else
                    {
                        hset.Add(rowvalue);
                    }
                }
            }
            else
            {
                columnname = string.Format("{0} {1}", tvo.UnionKey.First.Name, tvo.UnionKey.Second.Name);
                for (int i = 0; i < dataTable.Rows.Count; ++i)
                {
                    string rowvalue = string.Format("{0} {1}", dataTable.Rows[i][tvo.UnionKey.First.Name][0], dataTable.Rows[i][tvo.UnionKey.Second.Name][0]);
                    if (hset.Contains(rowvalue))
                    {
                        DebugMgr.LogError(string.Format("Error! 联合主键重复。tablename={0},fieldname={1},value={2}", tvo.Name, columnname, rowvalue));
                        return false;
                    }
                    else
                    {
                        hset.Add(rowvalue);
                    }
                }
            }


            return true;
        }
    }
}
