﻿using System.Collections.Generic;
using System.Xml;

namespace UNen.Table
{
    public class ModuleVO : ITableVerify
    {
        private string m_name = string.Empty;

        public string Name => m_name;

        private List<DiscreteVO> m_discretevolst = new List<DiscreteVO>();
        public int CountDiscreteVO(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_discretevolst.Count; ++i)
            {
                if (m_discretevolst[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }
        public List<DiscreteVO> FindDiscreteVOsByName(string name)
        {
            return m_discretevolst.FindAll(c => c.Name == name);
        }

        public List<DiscreteVO> FindAllDiscreteVO()
        {
            return m_discretevolst;
        }

        public bool Analyze(XmlNode node)
        {
            bool result = AttrUtils.GetAttr(node.Attributes["name"], out m_name, true);
            if (result == false)
            {
                DebugMgr.LogWarning(string.Format("表路径结构解析失败! modulename = {0}", m_name));
                return false;
            }

            XmlNodeList list = node.SelectNodes("table");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new DiscreteVO(this);
                result = vo.Analyze(list[i]);
                if (result == false)
                {
                    DebugMgr.LogError("Fail! module.table 表路径结构 解析失败");
                    return false;
                }
                m_discretevolst.Add(vo);
            }

            return true;
        }

        public bool VerifyStruct()
        {
            int cnt = ConfigMgr.tableConf.CountModule(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Fail! module名称重复 module.name = {0}", m_name));
                return false;
            }

            foreach (var vo in m_discretevolst)
            {
                if (vo.VerifyStruct() == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool VerifySelfData()
        {
            foreach (var vo in m_discretevolst)
            {
                if (vo.VerifySelfData() == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool VerifyLinkData()
        {
            foreach (var vo in m_discretevolst)
            {
                if (vo.VerifyLinkData() == false)
                {
                    return false;
                }
            }
            return true;
        }

    }
}
