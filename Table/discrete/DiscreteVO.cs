﻿using System.Xml;
using UKon.Data.Table;

namespace UNen.Table
{
    public class DiscreteVO : ITableVerify
    {
        private string m_name = string.Empty;
        private string m_path = string.Empty;
        private string m_keyrange = string.Empty;
        private TableVO m_table;
        public TableVO TableVO => m_table;

        private RangeVO<ulong> m_keyrangevo = null;
        public RangeVO<ulong> KeyRangeVO => m_keyrangevo;

        public string Name => m_name;
        public string Path => m_path;
        //public string KeyRange => m_keyrange;

        private LeafFieldVO keyfield;

        private AbstractDataTable dataTable = null;
        public AbstractDataTable GetDataTable()
        {
            return dataTable;
        }

        private ModuleVO m_owner;
        public ModuleVO Owner => m_owner;

        public DiscreteVO(ModuleVO owner)
        {
            m_owner = owner;
        }

        public bool Analyze(XmlNode node)
        {
            bool result = AttrUtils.GetAttr(node.Attributes["name"], out m_name, true);
            if (result == true)
            {
                result = AttrUtils.GetAttr(node.Attributes["path"], out m_path, true);
            }
            if (result == true)
            {
                result = AttrUtils.GetAttr(node.Attributes["keyrange"], out m_keyrange);
            }

            return result;
        }

        public bool VerifyStruct()
        {
            m_table = ConfigMgr.tableConf.FindVO(m_name);
            if (m_table == null)
            {
                DebugMgr.LogError(string.Format("Error！ 表结构不存在。 module.table.name={0}", m_name));
                DebugMgr.WriteLine();
                return false;
            }

            if (m_keyrange != string.Empty)
            {
                string[] ss = m_keyrange.Split(',');
                if (ss.Length != 2)
                {
                    DebugMgr.LogError(string.Format("Error！ keyrange字段的格式必须是“min,max”。 module.table.name={0}", m_name));
                    DebugMgr.WriteLine();
                    return false;
                }

                if (m_table.PrimaryKey != null)
                {
                    keyfield = m_table.PrimaryKey;
                }
                else
                {
                    keyfield = m_table.UnionKey.First;
                }

                if (SysDefine.CheckType(keyfield.Type.Value.ToLower(), SysDefine.ValueTypeConst))
                {
                    ulong min = ulong.Parse(ss[0]);
                    ulong max = ulong.Parse(ss[1]);
                    if (min > max)
                    {
                        DebugMgr.LogError(string.Format("Error！ keyrange字段的格式必须是“min,max”，且min不能大于max。 module.table.name={0}", m_name));
                        DebugMgr.WriteLine();
                        return false;
                    }
                    m_keyrangevo = new RangeVO<ulong>(min, max);
                }
            }

            return true;
        }

        public bool VerifySelfData()
        {
            var filePath = System.IO.Path.Combine(ConfigMgr.sysconf.SrcProjPath, m_path);

            int cnt = m_owner.CountDiscreteVO(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error! module.table重复 module.table.name = {0}", m_name));
                DebugMgr.WriteLine();
                return false;
            }

            if (System.IO.File.Exists(filePath) == false)
            {
                DebugMgr.LogError("Error！  文件不存在。");
                DebugMgr.LogError(string.Format("\t module.table.name = {0}, path={1}", m_name, m_path));
                DebugMgr.WriteLine();
                return false;
            }


            //验证分表数据有效性
            bool result = CSVDataTableAgent.Parse(filePath, m_table, out dataTable);
            if (result == false)
            {
                return false;
            }

            if (m_keyrangevo != null)
            {
                for (int i = 0; i < dataTable.Rows.Count; ++i)
                {
                    var row = dataTable.Rows[i];
                    ulong value = ulong.Parse(row[keyfield.Name][0].Value);

                    if (value >= m_keyrangevo.Min && value < m_keyrangevo.Max)
                    {
                        continue;
                    }
                    DebugMgr.LogError("Error!  module.table有数据不在keyrange定义的范围内。");
                    DebugMgr.LogError(string.Format("\tmodule.table name = {0}, path={1};", m_name, m_path));
                    DebugMgr.LogError(string.Format("\trange.min = {0}, range.max={1};", m_keyrangevo.Min, m_keyrangevo.Max));
                    DebugMgr.LogError(string.Format("\tcurrvalue = {0}. ", value));
                    DebugMgr.WriteLine();
                    return false;
                }
            }

            return true;
        }

        public bool VerifyLinkData()
        {
            //没有外部数据需要验证
            throw new System.NotImplementedException();
        }
    }
}
