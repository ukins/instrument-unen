using FacadeLib.Config

namespace FacadeLib
{
	public partial class TableMgr
	{
		private readonly ItemTable m_ItemTable = new ItemTable();
		public ItemTable ItemTableVO { get { return m_ItemTable; } }

		private readonly AwardTable m_AwardTable = new AwardTable();
		public AwardTable AwardTableVO { get { return m_AwardTable; } }

		private readonly SkillTable m_SkillTable = new SkillTable();
		public SkillTable SkillTableVO { get { return m_SkillTable; } }

		private readonly WndTable m_WndTable = new WndTable();
		public WndTable WndTableVO { get { return m_WndTable; } }


		public void Init()
		{
			m_tabledict.Add(m_ItemTable.Name, m_ItemTable);
			m_tablelist.Add(m_ItemTable);

			m_tabledict.Add(m_AwardTable.Name, m_AwardTable);
			m_tablelist.Add(m_AwardTable);

			m_tabledict.Add(m_SkillTable.Name, m_SkillTable);
			m_tablelist.Add(m_SkillTable);

			m_tabledict.Add(m_WndTable.Name, m_WndTable);
			m_tablelist.Add(m_WndTable);

		}
	}
}
