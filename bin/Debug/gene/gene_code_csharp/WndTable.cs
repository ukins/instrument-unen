using System;
using System.Collections.Generic;
using System.IO;

namespace FacadeLib.Config
{
	public class WndTableConf : SingleKeyConf<uint>
	{
		public override uint PrimaryKey { get { return m_ID; } }

		private uint m_id;
		public uint ID { get { return m_id; } }

		private string m_name;
		public string Name { get { return m_name; } }

		private string m_respath;
		public string ResPath { get { return m_respath; } }

		private ushort m_layer;
		public ushort Layer { get { return m_layer; } }

		private List<Tuple<>> m_preload = new List<Tuple<>>();
		public List<Tuple<>> Preload { get { return m_Preload; } }


		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();
				m_name = br.ReadString();
				m_respath = br.ReadString();
				m_layer = br.ReadUInt16();

				m_preload.Clear();
				int len = br.ReadInt32();
				for (int i = 0; i < len; ++i)
				{
					Tuple<> vo = Tuple.Create(

	);
					m_preload.Add(vo);
				}
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Deserialize(string br)
		{
			string[] fields = br.Split(',');

			if (fields[0].StartsWith("#") || fields[0] == "&&")
			{
				return false;
			}

			try
			{
			int i = 1;
				m_id = Convert.ToUInt32(fields[i++]);
				m_name = fields[i++];
				m_respath = fields[i++];
				m_layer = Convert.ToUInt16(fields[i++]);

				m_preload.Clear();
					Tuple<> vo = Tuple.Create(

	);
					m_preload.Add(vo);
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Append(AbstractItem item)
		{
			var conf = item as WndTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			m_preload.AddRange(conf.m_preload)
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Clone(AbstractItem item)
		{
			var conf = item as WndTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			m_ID = conf.m_ID;
				m_id = conf.m_id;
			m_Name = conf.m_Name;
				m_name = conf.m_name;
			m_ResPath = conf.m_ResPath;
				m_respath = conf.m_respath;
			m_Layer = conf.m_Layer;
				m_layer = conf.m_layer;
			m_Preload = conf.m_Preload;

				m_preload.Clear();
				m_preload.AddRange(conf.m_preload);
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool LinkForeignTable()
		{
			return true;
		}

	}

	public class WndTable : SingleKeyTable<WndTableConf,uint>
	{
		private readonly string m_name = "WndTable";
		public override string Name { get { return m_name; } }
		private readonly List<string> m_path = new List<string>()
		{
			@"generic/cfg/wnd.csv",
		};
		public override List<string> Path { get { return m_path; } }
	}
}
