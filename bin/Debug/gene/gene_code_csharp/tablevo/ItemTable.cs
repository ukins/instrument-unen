using System;
using System.Collections.Generic;
using System.IO;

namespace FacadeLib.Config
{
	public class ItemTableConf : SingleKeyConf<uint>
	{
		public override uint PrimaryKey { get { return m_ID; } }

		private uint m_id;
		public uint ID { get { return m_id; } }

		private string m_name;
		public string Name { get { return m_name; } }

		private string m_desc;
		public string Desc { get { return m_desc; } }


		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();
				m_name = br.ReadString();
				m_desc = br.ReadString();
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Deserialize(string br)
		{
			string[] fields = br.Split(',');

			if (fields[0].StartsWith("#") || fields[0] == "&&")
			{
				return false;
			}

			try
			{
			int i = 1;
				m_id = Convert.ToUInt32(fields[i++]);
				m_name = fields[i++];
				m_desc = fields[i++];
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Append(AbstractItem item)
		{
			var conf = item as ItemTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Clone(AbstractItem item)
		{
			var conf = item as ItemTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			m_ID = conf.m_ID;
				m_id = conf.m_id;
			m_Name = conf.m_Name;
				m_name = conf.m_name;
			m_Desc = conf.m_Desc;
				m_desc = conf.m_desc;
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool LinkForeignTable()
		{
			return true;
		}

	}

	public class ItemTable : SingleKeyTable<ItemTableConf,uint>
	{
		private readonly string m_name = "ItemTable";
		public override string Name { get { return m_name; } }
		private readonly List<string> m_path = new List<string>()
		{
			@"generic/cfg/item.csv",
			@"flogin/cfg/item.csv",
			@"flobby/cfg/item.csv",
		};
		public override List<string> Path { get { return m_path; } }
	}
}
