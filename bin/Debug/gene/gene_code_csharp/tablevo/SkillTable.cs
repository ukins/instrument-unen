using System;
using System.Collections.Generic;
using System.IO;

namespace FacadeLib.Config
{
	public class SkillTableConf : UnionKeyConf<uint,uint>
	{
		public override uint UnionKey1 { get { return m_SkillId; } }

		public override uint UnionKey2 { get { return m_SkillLv; } }

		private uint m_skillid;
		public uint SkillId { get { return m_skillid; } }

		private uint m_skilllv;
		public uint SkillLv { get { return m_skilllv; } }

		private string m_desc;
		public string Desc { get { return m_desc; } }


		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_skillid = br.ReadUInt32();
				m_skilllv = br.ReadUInt32();
				m_desc = br.ReadString();
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Deserialize(string br)
		{
			string[] fields = br.Split(',');

			if (fields[0].StartsWith("#") || fields[0] == "&&")
			{
				return false;
			}

			try
			{
			int i = 1;
				m_skillid = Convert.ToUInt32(fields[i++]);
				m_skilllv = Convert.ToUInt32(fields[i++]);
				m_desc = fields[i++];
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Append(AbstractItem item)
		{
			var conf = item as SkillTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Clone(AbstractItem item)
		{
			var conf = item as SkillTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			m_SkillId = conf.m_SkillId;
				m_skillid = conf.m_skillid;
			m_SkillLv = conf.m_SkillLv;
				m_skilllv = conf.m_skilllv;
			m_Desc = conf.m_Desc;
				m_desc = conf.m_desc;
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool LinkForeignTable()
		{
			return true;
		}
	}

	public class SkillTable : UnionKeyTable<SkillTableConf,uint,uint>
	{
		private readonly string m_name = "SkillTable";
		public override string Name { get { return m_name; } }
		private readonly List<string> m_path = new List<string>()
		{
			@"generic/cfg/skill.csv",
			@"flobby/cfg/skill.csv",
		};
		public override List<string> Path { get { return m_path; } }
	}
}
