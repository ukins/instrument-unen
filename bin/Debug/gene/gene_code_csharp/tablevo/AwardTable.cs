using System;
using System.Collections.Generic;
using System.IO;

namespace FacadeLib.Config
{
	public class AwardTableConf : SingleKeyConf<uint>
	{
		public override uint PrimaryKey { get { return m_ID; } }

		private uint m_id;
		public uint ID { get { return m_id; } }

		private List<Tuple<>> m_reward = new List<Tuple<>>();
		public List<Tuple<>> Reward { get { return m_Reward; } }


		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				m_id = br.ReadUInt32();

				m_reward.Clear();
				int len = br.ReadInt32();
				for (int i = 0; i < len; ++i)
				{
					Tuple<> vo = Tuple.Create(

	);
					m_reward.Add(vo);
				}
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Deserialize(string br)
		{
			string[] fields = br.Split(',');

			if (fields[0].StartsWith("#") || fields[0] == "&&")
			{
				return false;
			}

			try
			{
			int i = 1;
				m_id = Convert.ToUInt32(fields[i++]);

				m_reward.Clear();
					Tuple<> vo = Tuple.Create(

	);
					m_reward.Add(vo);
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Append(AbstractItem item)
		{
			var conf = item as AwardTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			m_reward.AddRange(conf.m_reward)
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool Clone(AbstractItem item)
		{
			var conf = item as AwardTableConf
			if (conf == null)
			{
				return false;
			}

			try
			{
			m_ID = conf.m_ID;
				m_id = conf.m_id;
			m_Reward = conf.m_Reward;

				m_reward.Clear();
				m_reward.AddRange(conf.m_reward);
			}
			catch (Exception e)
			{
				DebugMgr.LogError(e.Message);
				return false;
			}
			return true;
		}

		public override bool LinkForeignTable()
		{
			for (int i = 0; i < m_reward.Count; ++i)
			{
				m_reward[i].Item2.Deserialize(TableMgr.Instance.ItemTableVO.FindVO(m_reward[i].Item1));
			}

			return true;
		}

	}

	public class AwardTable : SingleKeyTable<AwardTableConf,uint>
	{
		private readonly string m_name = "AwardTable";
		public override string Name { get { return m_name; } }
		private readonly List<string> m_path = new List<string>()
		{
			@"flobby/cfg/award.csv",
		};
		public override List<string> Path { get { return m_path; } }
	}
}
