﻿using System.Collections.Generic;

namespace UNen
{
    public class ConfigMgr : Singleton<ConfigMgr>
    {
        public delegate bool ConfOperate();

        public static readonly SysConf sysconf = new SysConf();
        public static readonly TableGenericConf tableGenericConf = new TableGenericConf();
        public static readonly TableConf tableConf = new TableConf();
        public static readonly ProtocolConf protoConf = new ProtocolConf();

        private List<ConfOperate> m_list = new List<ConfOperate>()
        {
            sysconf.Load,
            sysconf.Analyze,
            tableGenericConf.Load,
            tableGenericConf.Analyze,
            tableGenericConf.Verify,
            tableConf.Load,
            MergeTableXml,
            tableConf.Analyze,
            tableConf.VerifyStruct,
            protoConf.Load,
            protoConf.Analyze,
            protoConf.Verify,
            tableConf.VerifySelfData,
            tableConf.VerifyLinkData
        };

        private static bool MergeTableXml()
        {
            return AttrUtils.MergeTableXml(tableGenericConf.XmlDoc, tableConf.XmlDoc);
        }

        public bool Init()
        {

            for (int i = 0; i < m_list.Count; ++i)
            {
                if (m_list[i].Invoke() == false)
                {
                    DebugMgr.Log(string.Format("Interrupted by {0}", i));
                    return false;
                }
            }
            return true;
        }

    }
}
