﻿using System.Collections.Generic;
using System.Xml;
using UNen.DT;
using UNen.Table;

namespace UNen
{
    public class TableConf : ITableVerify
    {
        public static readonly string FILE_NAME = "table.config";
        private static string ProjPath = string.Empty;

        private XmlDocument xmldoc = null;
        public XmlDocument XmlDoc => xmldoc;

        private List<DITableVO> m_tablevos = new List<DITableVO>();

        public List<DITableVO> FindAll()
        {
            return m_tablevos;
        }

        public DITableVO FindVO(string name)
        {
            return m_tablevos.Find(c => c.Name == name);
        }

        public int CountVO(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_tablevos.Count; ++i)
            {
                if (m_tablevos[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        private List<ModuleVO> m_modules = new List<ModuleVO>();
        public int CountModule(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_modules.Count; ++i)
            {
                if (m_modules[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public List<DiscreteVO> FindDiscreteVOByTableName(string tablename)
        {
            List<DiscreteVO> lst = new List<DiscreteVO>();
            foreach (var module in m_modules)
            {
                lst.AddRange(module.FindDiscreteVOsByName(tablename));
            }
            return lst;
        }

        public DiscreteVO FindDiscreteVOByTablePath(string path)
        {
            foreach (var module in m_modules)
            {
                var lst = module.FindAllDiscreteVO();
                foreach (var vo in lst)
                {
                    if (vo.Path == path)
                    {
                        return vo;
                    }
                }
            }
            return null;
        }

        public List<DiscreteVO> FindAllDiscreteVO()
        {
            List<DiscreteVO> lst = new List<DiscreteVO>();
            foreach (var module in m_modules)
            {
                lst.AddRange(module.FindAllDiscreteVO());
            }
            return lst;
        }

        //public bool Init()
        //{
        //    bool result = Load();
        //    if (result == false)
        //    {
        //        return false;
        //    }
        //    return Analyze();
        //}

        public bool Load()
        {
            string path = ConfigMgr.sysconf.AppPath;
            string filepath = System.IO.Path.Combine(path, FILE_NAME);
            xmldoc = new XmlDocument();
            try
            {
                xmldoc.Load(filepath);
            }
            catch (XmlException e)
            {
                DebugMgr.LogWarning(e.Message);
                return false;
            }
            return true;
        }

        public bool Analyze()
        {
            var xmlelement = xmldoc.SelectSingleNode("/config/tables");

            m_tablevos.Clear();

            XmlNodeList list = null;
            bool result = true;

            list = xmlelement.SelectNodes("table");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new DITableVO();
                vo.Parse(list[i]);
                m_tablevos.Add(vo);
            }

            xmlelement = xmldoc.SelectSingleNode("/config/project");
            result = AttrUtils.GetAttr(xmlelement.Attributes["path"], out string projpath);
            if (result == false)
            {
                DebugMgr.LogError("Fail! project path 解析失败");
                return false;
            }
            result = ConfigMgr.sysconf.CoverSrcProjPath(projpath);
            if (result == false)
            {
                DebugMgr.LogError("Fail! project path 无效");
                return false;
            }

            list = xmlelement.SelectNodes("module");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new ModuleVO();
                result = vo.Analyze(list[i]);
                if (result == false)
                {
                    DebugMgr.LogError("Fail! table module 解析失败");
                    return false;
                }
                m_modules.Add(vo);
            }


            //DebugMgr.Log("Succ! table.config 解析成功");
            return true;
        }

        public bool VerifyStruct()
        {
            for (int i = 0; i < m_modules.Count; ++i)
            {
                if (m_modules[i].VerifyStruct() == false)
                {
                    return false;
                }
            }

            for (int i = 0; i < m_tablevos.Count; ++i)
            {
                if (m_tablevos[i].CheckStruct() == false)
                {
                    return false;
                }
            }

            DebugMgr.Log("Succ! table.config 解析成功");
            return true;
        }

        public bool VerifySelfData()
        {
            for (int i = 0; i < m_modules.Count; ++i)
            {
                if (m_modules[i].VerifySelfData() == false)
                {
                    return false;
                }
            }

            for (int i = 0; i < m_tablevos.Count; ++i)
            {
                if (m_tablevos[i].CheckInternalData() == false)
                {
                    return false;
                }
            }

            DebugMgr.Log("Succ! table.config 表内数据验证成功");
            return true;
        }

        public bool VerifyLinkData()
        {
            for (int i = 0; i < m_tablevos.Count; ++i)
            {
                if (m_tablevos[i].CheckExternalData() == false)
                {
                    return false;
                }
            }

            DebugMgr.Log("Succ! table.config 表间数据验证成功");
            return true;
        }

    }

}
