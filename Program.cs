﻿using System;

namespace UNen
{
    class Program
    {
        static void Main(string[] args)
        {
            DebugMgr.Init();

            ConfigMgr.Instance.Init();
            //if (result == false)
            //{
            //    return;
            //}

            //DebugMgr.Log("请输入指令...");

            string linestr = string.Empty;
            string[] strlst;

            while (true)
            {
                linestr = Console.ReadLine();
                linestr = linestr.Trim();
                if (linestr.Length == 0)
                {
                    continue;
                }
                if (string.CompareOrdinal(linestr.ToLower(), "cls") == 0)
                {
                    Console.Clear();
                    continue;
                }
                strlst = linestr.Split(' ');
                if (strlst.Length == 0)
                {
                    DebugMgr.LogWarning("Warning！ 该指令无法解析");
                    continue;
                }

                if (strlst[0] == "exit" || strlst[0] == "q")
                {
                    break;
                }
                CommandMgr.Instance.Process(strlst);
            }

            Console.ReadLine();
        }
    }
}
