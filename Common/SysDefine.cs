﻿using UKon.Data;

namespace UNen
{
    public delegate void ActionInt(int i);

    public enum EAppMode
    {
        Client,
        Server
    }

    /// <summary>
    /// 本程序所支持处理的数据类型
    /// </summary>
    public struct SupportFieldType
    {
        public static bool ContainComma(string value)
        {
            return value.IndexOf(',') >= 0;
        }

        public static string GetBinaryReaderMethodName(string votype, string param = "br")
        {
            string methodname = string.Empty;
            switch (votype)
            {
                case DTDefine.BOOL:
                    methodname = string.Format("{0}.ReadBoolean()", param);
                    break;
                case DTDefine.BYTE:
                    methodname = string.Format("{0}.ReadByte()", param);
                    break;
                case DTDefine.SHORT:
                    methodname = string.Format("{0}.ReadInt16()", param);
                    break;
                case DTDefine.USHORT:
                    methodname = string.Format("{0}.ReadUInt16()", param);
                    break;
                case DTDefine.INT:
                    methodname = string.Format("{0}.ReadInt32()", param);
                    break;
                case DTDefine.UINT:
                    methodname = string.Format("{0}.ReadUInt32()", param);
                    break;
                case DTDefine.LONG:
                    methodname = string.Format("{0}.ReadInt64()", param);
                    break;
                case DTDefine.ULONG:
                    methodname = string.Format("{0}.ReadUInt64()", param);
                    break;
                case DTDefine.FLOAT:
                    methodname = string.Format("{0}.ReadSingle()", param);
                    break;
                //case "text":
                case DTDefine.STRING:
                    methodname = string.Format("{0}.ReadString()", param);
                    break;
                case DTDefine.TIME:
                    methodname = string.Format("DateTimeOffset.Parse({0}.ReadString())", param);
                    break;
                case DTDefine.TIMESPAN:
                    methodname = string.Format("TimeSpan.Parse({0}.ReadString())", param);
                    break;
                //case POINT:
                //    methodname = string.Format("ChessPoint.Parse({0}.ReadString())", param);
                //    break;
                case DTDefine.VECTOR2:
                    methodname = string.Format("UVector2.Parse({0}.ReadString())", param);
                    break;
                case DTDefine.VECTOR2INT:
                    methodname = string.Format("UVector2Int.Parse({0}.ReadString())", param);
                    break;
                case DTDefine.VECTOR3:
                    methodname = string.Format("UVector3.Parse({0}.ReadString())", param);
                    break;
                case DTDefine.VECTOR3INT:
                    methodname = string.Format("UVector3Int.Parse({0}.ReadString())", param);
                    break;
                default: break;
            }
            return methodname;
        }

        public static string GetStringReaderMethodName(string votype, string param)
        {
            string methodname = string.Empty;
            switch (votype)
            {
                case DTDefine.BOOL:
                    methodname = string.Format("{0}.ToLower() == \"true\" ? true : false", param);
                    break;
                case DTDefine.BYTE:
                    methodname = string.Format("{0} == string.Empty ? (byte)0 : byte.Parse({0})", param);
                    break;
                case DTDefine.SHORT:
                    methodname = string.Format("{0} == string.Empty ? (Int16)0 : Int16.Parse({0})", param);
                    break;
                case DTDefine.USHORT:
                    methodname = string.Format("{0} == string.Empty ? (UInt16)0 : UInt16.Parse({0})", param);
                    break;
                case DTDefine.INT:
                    methodname = string.Format("{0} == string.Empty ? (Int32)0 : Int32.Parse({0})", param);
                    break;
                case DTDefine.UINT:
                    methodname = string.Format("{0} == string.Empty ? 0 : UInt32.Parse({0})", param);
                    break;
                case DTDefine.LONG:
                    methodname = string.Format("{0} == string.Empty ? (Int64)0 : Int64.Parse({0})", param);
                    break;
                case DTDefine.ULONG:
                    methodname = string.Format("{0} == string.Empty ? (UInt64)0 : UInt64.Parse({0})", param);
                    break;
                case DTDefine.FLOAT:
                    methodname = string.Format("{0} == string.Empty ? 0f : float.Parse({0})", param);
                    break;
                //case "text":
                case DTDefine.STRING:
                    methodname = string.Format("{0}", param);
                    break;
                case DTDefine.TIME:
                    methodname = string.Format("DateTimeOffset.Parse({0})", param);
                    break;
                case DTDefine.TIMESPAN:
                    methodname = string.Format("TimeSpan.Parse({0})", param);
                    break;
                //case POINT:
                //    methodname = string.Format("ChessPoint.Parse({0})", param);
                //    break;
                case DTDefine.VECTOR2:
                    methodname = string.Format("UVector2.Parse({0})", param);
                    break;
                case DTDefine.VECTOR2INT:
                    methodname = string.Format("UVector2Int.Parse({0})", param);
                    break;
                case DTDefine.VECTOR3:
                    methodname = string.Format("UVector3.Parse({0})", param);
                    break;
                case DTDefine.VECTOR3INT:
                    methodname = string.Format("UVector3Int.Parse({0})", param);
                    break;
                default: break;
            }
            return methodname;
        }



    }

    public class SysDefine
    {
        public static readonly string[] ElementTypeConst = {
            DTDefine.BOOL,
            DTDefine.BYTE,
            DTDefine.SHORT,
            DTDefine.USHORT,
            DTDefine.INT,
            DTDefine.UINT,
            DTDefine.LONG,
            DTDefine.ULONG,
            DTDefine.FLOAT,
            DTDefine.STRING,
            DTDefine.TIME,
            DTDefine.TIMESPAN,
            DTDefine.VECTOR2,
            DTDefine.VECTOR2INT,
            DTDefine.VECTOR3,
            DTDefine.VECTOR3INT,
            DTDefine.TUPLE,
            //DTDefine.POINT,
            //DTDefine.TEXT
        };

        public static readonly string[] ValueTypeConst ={
            DTDefine.BYTE,
            DTDefine.SHORT,
            DTDefine.USHORT,
            DTDefine.INT,
            DTDefine.UINT,
            DTDefine.LONG,
            DTDefine.ULONG,
            DTDefine.FLOAT,
            DTDefine.VECTOR2,
            DTDefine.VECTOR2INT,
            DTDefine.VECTOR3,
            DTDefine.VECTOR3INT,
        };

        public static readonly string[] ProtoTypeConst =
        {
            DTDefine.BOOL,
            DTDefine.BYTE,
            DTDefine.SHORT,
            DTDefine.USHORT,
            DTDefine.INT,
            DTDefine.UINT,
            DTDefine.LONG,
            DTDefine.ULONG,
            DTDefine.FLOAT,
            DTDefine.VECTOR2,
            DTDefine.VECTOR2INT,
            DTDefine.VECTOR3,
            DTDefine.VECTOR3INT,
            DTDefine.STRING,
        };

        public static bool CheckType(string type, string[] lst)
        {
            bool result = false;
            for (int i = 0; i < lst.Length; ++i)
            {
                if (type == lst[i])
                {
                    result = true;
                    break;
                }
            }
            return result;
        }


        public static bool CheckTypeIsDefined(string type)
        {
            int structcnt = ConfigMgr.tableGenericConf.GetStructCnt(type);
            int enumcnt = ConfigMgr.tableGenericConf.GetEnumCnt(type);
            bool issystype = CheckType(type, ElementTypeConst);
            if (structcnt == 0 && enumcnt == 0 && issystype == false)
            {
                return false;
            }
            return true;
        }

        public static readonly string ClientNamespace = "UTan";
        public static readonly string ServerNamespace = "UHan";
        public static bool isClient = true;
        public static string RootNamespace
        {
            get
            {
                if (isClient == true)
                {
                    return ClientNamespace;
                }
                return ServerNamespace;
            }
        }

    }
}
