﻿namespace UNen
{
    public class VOPair<T, K> 
        where T:new()
        where K:new()
    {
        public T First;
        public K Second;

        public VOPair()
        {

        }

        public VOPair(T t, K k)
        {
            First = t;
            Second = k;
        }

        public void Clear()
        {
            if (First is VOPool<T>)
            {
                VOPool<T>.Push(First);
            }

            if (Second is VOPool<K>)
            {
                VOPool<K>.Push(Second);
            }
        }
    }
}
