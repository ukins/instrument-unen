﻿using UKon.Data.Table;

namespace UNen
{
    public interface IStructDecorator : IXmlParse
    {
        bool CheckStruct();
    }
}
