﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon.Data;
using UKon.Data.Table;
using UNen.DT;

namespace UNen.Cmd
{
    class GeneTableUtils
    {
        /// <summary>
        /// 生成TableMgr的文件内容
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="tables"></param>
        public static void WriteTableMgr(StreamWriter sw, List<DITableVO> tables)
        {
            sw.WriteLine("using UKon.Table;");
            sw.WriteLine();
            sw.WriteLine("namespace {0}", SysDefine.RootNamespace);
            sw.WriteLine("{");
            sw.WriteLine("\tpublic partial class TableMgr");
            sw.WriteLine("\t{");

            foreach (var tvo in tables)
            {
                sw.WriteLine("\t\tprivate readonly {0} m_{0} = new {0}();", tvo.Name);
                sw.WriteLine("\t\tpublic {0} {0}VO {{ get {{ return m_{0}; }} }}", tvo.Name);
                sw.WriteLine();
            }

            sw.WriteLine();

            sw.WriteLine("\t\tprivate void InnerInit()");
            sw.WriteLine("\t\t{");
            foreach (var tvo in tables)
            {
                sw.WriteLine("\t\t\tm_tabledict.Add(m_{0}.Name, m_{0});", tvo.Name);
                sw.WriteLine("\t\t\tm_tablelist.Add(m_{0});", tvo.Name);
                sw.WriteLine();
            }
            sw.WriteLine("\t\t}");

            sw.WriteLine("\t}");
            sw.WriteLine("}");

        }

        private static string GetTupleType(List<IFieldVO> fields)
        {
            StringBuilder sb = new StringBuilder();

            for (int j = 0; j < fields.Count; ++j)
            {
                var leaf = fields[j] as DIFieldVO;
                var firsttype = fields[j].Type.FirstElementType;
                var enumvo = ConfigMgr.tableGenericConf.FindEnum(firsttype);
                var structvo = ConfigMgr.tableGenericConf.FindStruct(firsttype);

                if (enumvo != null || structvo != null)
                {
                    sb.Append(firsttype);
                    if (j != fields.Count - 1)
                    {
                        sb.Append(", ");
                    }
                }
                else
                {
                    sb.Append(firsttype == "text" ? "string" : DTDefine.GetTypeByString(firsttype));
                    if (j != fields.Count - 1)
                    {
                        sb.Append(", ");
                    }
                    if (leaf.ForeignTableVO != null)
                    {
                        sb.Append(leaf.ForeignTableVO.Name);
                        sb.Append("Conf");
                        if (j != fields.Count - 1)
                        {
                            sb.Append(", ");
                        }
                    }
                }
            }

            return sb.ToString();
        }

        private static string GetTupleConstructValue(List<IFieldVO> fields)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < fields.Count; ++i)
            {
                var leaffieldvo = fields[i] as DIFieldVO;
                var firsttype = fields[i].Type.FirstElementType;
                var enumvo = ConfigMgr.tableGenericConf.FindEnum(firsttype);
                var structvo = ConfigMgr.tableGenericConf.FindStruct(firsttype);

                if (enumvo != null)
                {
                    sb.Append(string.Format("\t\t\t\t\t\t({0})Enum.Parse(typeof({0}), {1})", enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("string")));
                    if (i != fields.Count - 1)
                    {
                        sb.Append(",\n");
                    }
                }
                else if (structvo != null)
                {
                    sb.Append(string.Format("\t\t\t\t\t\t{0}.CreateFromBinary(br)", structvo.Name));
                    if (i != fields.Count - 1)
                    {
                        sb.Append(",\n");
                    }
                }
                else
                {
                    sb.Append(string.Format("\t\t\t\t\t\t{0}", SupportFieldType.GetBinaryReaderMethodName(firsttype)));

                    if (i != fields.Count - 1 || leaffieldvo.ForeignTableVO != null)
                    {
                        sb.Append(",\n");
                    }
                    if (leaffieldvo.ForeignTableVO != null)
                    {
                        sb.Append(string.Format("\t\t\t\t\t\tnew {0}Conf()", leaffieldvo.ForeignTableVO.Name));
                        if (i != fields.Count - 1) { sb.Append(",\n"); }
                    }
                }
            }
            return sb.ToString();
        }

        private static void WriteFieldTupleDefine(StreamWriter sw, NodeFieldVO nodefield, EFieldType fieldtype)
        {
            List<IFieldVO> fields = nodefield.FindFields();

            string typelist = GetTupleType(fields);

            string privatefieldname = nodefield.FullName.ToLower().Replace('.', '_');
            string publicfieldname = nodefield.FullName.Replace('.', '_');

            switch (fieldtype)
            {
                case EFieldType.Single:
                    {
                        sw.WriteLine(string.Format("\t\tprivate Tuple<{0}> m_{1};", typelist, privatefieldname));
                        sw.WriteLine(string.Format("\t\tpublic Tuple<{0}> {1} {{ get {{ return m_{2}; }} }}", typelist, publicfieldname, privatefieldname));
                    }
                    break;
                case EFieldType.List:
                    {
                        sw.WriteLine(string.Format("\t\tprivate List<Tuple<{0}>> m_{1} = new List<Tuple<{0}>>();", typelist, privatefieldname));
                        sw.WriteLine(string.Format("\t\tpublic List<Tuple<{0}>> {1} {{ get {{ return m_{2}; }} }}", typelist, publicfieldname, privatefieldname));
                    }
                    break;
                case EFieldType.Array:
                    {
                        sw.WriteLine(string.Format("\t\tprivate Tuple<{0}>[] m_{1};", typelist, privatefieldname));
                        sw.WriteLine(string.Format("\t\tpublic Tuple<{0}>[] {1} {{ get {{ return m_{2}; }} }}", typelist, publicfieldname, privatefieldname));
                    }
                    break;
            }
        }

        private static void WriteTryBegin(StreamWriter sw)
        {
            sw.WriteLine("\t\t\ttry");
            sw.WriteLine("\t\t\t{");
        }

        private static void WriteTryEnd(StreamWriter sw, string retstr)
        {
            sw.WriteLine("\t\t\t}");
            sw.WriteLine("\t\t\tcatch (Exception e)");
            sw.WriteLine("\t\t\t{");
            sw.WriteLine("\t\t\t\tLogMgr.Instance.LogError(e.ToString());");
            sw.WriteLine("\t\t\t\treturn {0};", retstr);
            sw.WriteLine("\t\t\t}");
        }

        private static void WriteFieldDefine(StreamWriter sw, List<IFieldVO> fields)
        {
            ActionInt WriteLineFunc = delegate (int i)
            {
                if (i < fields.Count - 1)
                    sw.WriteLine();
            };

            for (int i = 0; i < fields.Count; ++i)
            {
                //常规字段
                if (fields[i] is DIFieldVO)
                {
                    var leaffieldvo = fields[i] as DIFieldVO;
                    var fieldtype = leaffieldvo.Type.FieldType;
                    var name_lower = leaffieldvo.Name.ToLower();
                    var typeparser = leaffieldvo.Type;
                    if (fieldtype == EFieldType.Single)
                    {
                        sw.WriteLine("\t\tprivate {0} m_{1};", DTDefine.GetTypeByString(leaffieldvo.Type.Value), name_lower);
                        sw.WriteLine("\t\tpublic {0} {1} {{ get {{ return m_{2}; }} }}", DTDefine.GetTypeByString(leaffieldvo.Type.Value), leaffieldvo.Name, name_lower);
                        WriteLineFunc(i);
                        if (leaffieldvo.ForeignTableVO != null)
                        {
                            var tablevo = leaffieldvo.ForeignTableVO;
                            sw.WriteLine("\t\tprivate {0}Conf m_{1}_conf;", tablevo.Name, name_lower);
                            //sw.WriteLine("\t\tpublic {0}Conf {1}Conf {{ get {{ return m_{2}_conf; }} }}", tablevo.Name, leaffieldvo.Name, leaffieldvo.Name.ToLower());
                            sw.WriteLine("\t\tpublic {0}Conf {1}Conf", tablevo.Name, leaffieldvo.Name);
                            sw.WriteLine("\t\t{");
                            sw.WriteLine("\t\t\tget");
                            sw.WriteLine("\t\t\t{");
                            sw.WriteLine("\t\t\t\tif (m_{0}_conf == null)", name_lower);
                            sw.WriteLine("\t\t\t\t{");
                            sw.WriteLine("\t\t\t\t\tm_{0}_conf = TableMgr.Instance.{1}VO.GenericFindVO({2});", name_lower, tablevo.Name, leaffieldvo.Name);
                            sw.WriteLine("\t\t\t\t}");
                            sw.WriteLine("\t\t\t\treturn m_{0}_conf;", name_lower);
                            sw.WriteLine("\t\t\t}");
                            sw.WriteLine("\t\t}");
                            WriteLineFunc(i);
                        }
                    }
                    else if (fieldtype == EFieldType.List)
                    {
                        sw.WriteLine("\t\tprivate List<{0}> m_{1};", DTDefine.GetTypeByString(leaffieldvo.Type.FirstElementType), leaffieldvo.Name.ToLower());
                        sw.WriteLine("\t\tpublic List<{0}> {1} {{ get {{ return m_{2}; }} }}", DTDefine.GetTypeByString(leaffieldvo.Type.FirstElementType), leaffieldvo.Name, leaffieldvo.Name.ToLower());
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.Array)
                    {
                        sw.WriteLine("\t\tprivate {0}[] m_{1};", DTDefine.GetTypeByString(leaffieldvo.Type.FirstElementType), leaffieldvo.Name.ToLower());
                        sw.WriteLine("\t\tpublic {0}[] {1} {{ get {{ return m_{2}; }} }}", DTDefine.GetTypeByString(leaffieldvo.Type.FirstElementType), leaffieldvo.Name, leaffieldvo.Name.ToLower());
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.Dict)
                    {
                        DebugMgr.LogWarning("WriteFieldDefine：字典类型待处理");
                    }
                }
                else
                {
                    var nodefield = fields[i] as DINodeVO;
                    var fieldtype = nodefield.Type.FieldType;
                    var istuple = nodefield.Type.FirstElementType == DTDefine.TUPLE;
                    if (fieldtype == EFieldType.Single)
                    {
                        if (istuple == false)
                        {
                            sw.WriteLine("\t\tprivate {0} m_{1} = new {0}();", DTDefine.GetTypeByString(nodefield.Type.Value), nodefield.Name.ToLower());
                            sw.WriteLine("\t\tpublic {0} {1} {{ get {{ return m_{2}; }} }}", DTDefine.GetTypeByString(nodefield.Type.Value), nodefield.Name, nodefield.Name.ToLower());
                        }
                        else
                        {
                            WriteFieldTupleDefine(sw, nodefield, fieldtype);
                        }
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.List)
                    {
                        if (istuple == false)
                        {
                            sw.WriteLine("\t\tprivate List<{0}> m_{1};", DTDefine.GetTypeByString(nodefield.Type.FirstElementType), nodefield.Name.ToLower());
                            sw.WriteLine("\t\tpublic List<{0}> {1} {{ get {{ return m_{2}; }} }}", DTDefine.GetTypeByString(nodefield.Type.FirstElementType), nodefield.Name, nodefield.Name.ToLower());
                        }
                        else
                        {
                            WriteFieldTupleDefine(sw, nodefield, fieldtype);
                        }
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.Array)
                    {
                        if (istuple == false)
                        {
                            sw.WriteLine("\t\tprivate {0}[] m_{1};", DTDefine.GetTypeByString(nodefield.Type.FirstElementType), nodefield.Name.ToLower());
                            sw.WriteLine("\t\tpublic {0}[] {1} {{ get {{ return m_{2}; }} }}", DTDefine.GetTypeByString(nodefield.Type.FirstElementType), nodefield.Name, nodefield.Name.ToLower());
                        }
                        else
                        {
                            WriteFieldTupleDefine(sw, nodefield, fieldtype);
                        }
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.Dict)
                    {
                        DebugMgr.LogWarning("WriteFieldDefine：字典类型待处理");
                    }
                }
            }
        }

        private static void WriteFieldTupleDeserializeByBinary(StreamWriter sw, NodeFieldVO nodefield, EFieldType fieldtype)
        {
            List<IFieldVO> fields = nodefield.FindFields();

            string privatefieldname = nodefield.FullName.ToLower().Replace('.', '_');
            string publicfieldname = nodefield.FullName.Replace('.', '_');
            string typelist = GetTupleType(fields);
            string valuelist = GetTupleConstructValue(fields);

            switch (fieldtype)
            {
                case EFieldType.Single:
                    {
                        sw.WriteLine("\t\t\t\tm_{0} = Tuple.Create(\n{1});", privatefieldname, valuelist);
                    }
                    break;
                case EFieldType.List:
                    {
                        sw.WriteLine("\t\t\t\tm_{0}.Clear();", privatefieldname);
                        sw.WriteLine("\t\t\t\tint len = br.ReadInt32();");
                        sw.WriteLine("\t\t\t\tfor (int i = 0; i < len; ++i)");
                        sw.WriteLine("\t\t\t\t{");
                        sw.WriteLine("\t\t\t\t\tTuple<{0}> vo = Tuple.Create(\n{1});", typelist, valuelist);
                        sw.WriteLine("\t\t\t\t\tm_{0}.Add(vo);", privatefieldname);
                        sw.WriteLine("\t\t\t\t}");
                    }
                    break;
                case EFieldType.Array:
                    {
                        sw.WriteLine("\t\t\t\tint len = br.ReadInt32();");
                        sw.WriteLine("\t\t\t\tm_{0} = new Tuple<{1}>[len];", privatefieldname, typelist);
                        sw.WriteLine("\t\t\t\tfor (int i = 0; i < len; ++i)");
                        sw.WriteLine("\t\t\t\t{");
                        sw.WriteLine("\t\t\t\t\tTuple<{0}> vo = Tuple.Create(\n{1});", typelist, valuelist);
                        sw.WriteLine("\t\t\t\t\tm_{0}[i] = vo;", privatefieldname);
                        sw.WriteLine("\t\t\t\t}");
                    }
                    break;
            }
            //sw.WriteLine("\t\t\t\tm_shelldict.Add(\"{0}\", m_{1});", publicfieldname, privatefieldname);


        }

        private static void WriteFieldDeserializeByBinary(StreamWriter sw, List<IFieldVO> fields)
        {
            sw.WriteLine("\t\tpublic override bool Deserialize(BinaryReader br)");
            sw.WriteLine("\t\t{");
            WriteTryBegin(sw);

            ActionInt WriteLineFunc = delegate (int i)
            {
                if (i < fields.Count - 1)
                    sw.WriteLine();
            };

            for (int i = 0; i < fields.Count; ++i)
            {
                if (fields[i] is DIFieldVO)
                {
                    var leaffieldvo = fields[i] as DIFieldVO;
                    var fieldtype = leaffieldvo.Type.FieldType;
                    var name_lower = leaffieldvo.Name.ToLower();
                    var enumvo = ConfigMgr.tableGenericConf.FindEnum(leaffieldvo.Type.FirstElementType);
                    var structvo = ConfigMgr.tableGenericConf.FindStruct(leaffieldvo.Type.FirstElementType);
                    if (fieldtype == EFieldType.Single)
                    {
                        if (enumvo != null)
                        {
                            //(ESortingLayer)Enum.Parse(typeof(ESortingLayer), value);
                            sw.WriteLine("\t\t\t\tm_{0} = ({1})Enum.Parse(typeof({1}), {2});", name_lower, enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("string"));
                        }
                        else if (structvo != null && structvo.Extern == true)
                        {
                            sw.WriteLine("\t\t\t\tm_{0} = new {1}();", name_lower, structvo.Name);
                            sw.WriteLine("\t\t\t\tm_{0}.Deserialize(br);", name_lower);
                        }
                        else
                        {
                            sw.WriteLine("\t\t\t\tm_{0} = {1};", name_lower, SupportFieldType.GetBinaryReaderMethodName(leaffieldvo.Type.FirstElementType));
                        }
                        if (leaffieldvo.ForeignTableVO != null)
                        {
                            sw.WriteLine("\t\t\t\tm_{0}_conf = null;", name_lower);
                        }
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.List)
                    {
                        sw.WriteLine("\t\t\t\tint m_{0}_cnt = br.ReadInt32();", name_lower);
                        sw.WriteLine("\t\t\t\tm_{0} = new List<{1}>();", name_lower, DTDefine.GetTypeByString(leaffieldvo.Type.FirstElementType));
                        sw.WriteLine("\t\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", name_lower);
                        sw.WriteLine("\t\t\t\t{");
                        if (enumvo != null)
                        {
                            sw.WriteLine("\t\t\t\t\tm_{0}.Add(({1}){2});", name_lower, enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("int"));
                        }
                        else if (structvo != null && structvo.Extern == true)
                        {
                            sw.WriteLine("\t\t\t\t\tm_{0}[i].Add(new {1}());", name_lower, structvo.Name);
                            sw.WriteLine("\t\t\t\t\tm_{0}[i].Deserialize(br);", name_lower);
                        }
                        else
                        {
                            sw.WriteLine("\t\t\t\t\tm_{0}.Add({1});", name_lower, SupportFieldType.GetBinaryReaderMethodName(leaffieldvo.Type.FirstElementType));
                        }
                        sw.WriteLine("\t\t\t\t}");
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.Array)
                    {
                        sw.WriteLine("\t\t\t\tint m_{0}_cnt = br.ReadInt32();", name_lower);
                        sw.WriteLine("\t\t\t\tm_{0} = new {1}[m_{0}_cnt];", name_lower, DTDefine.GetTypeByString(leaffieldvo.Type.FirstElementType));
                        sw.WriteLine("\t\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", name_lower);
                        sw.WriteLine("\t\t\t\t{");
                        if (enumvo != null)
                        {
                            sw.WriteLine("\t\t\t\t\tm_{0}[i] = ({1}){2};", name_lower, enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("int"));
                        }
                        else if (structvo != null && structvo.Extern == true)
                        {
                            sw.WriteLine("\t\t\t\t\tm_{0}[i] = new {1}();", name_lower, structvo.Name);
                            sw.WriteLine("\t\t\t\t\tm_{0}[i].Deserialize(br);", name_lower);
                        }
                        else
                        {
                            sw.WriteLine("\t\t\t\t\tm_{0}[i] = {1};", name_lower, SupportFieldType.GetBinaryReaderMethodName(leaffieldvo.Type.FirstElementType));
                        }
                        sw.WriteLine("\t\t\t\t}");
                        WriteLineFunc(i);
                    }
                    else
                    {
                        DebugMgr.LogWarning("WriteFieldDeserializeByBinary：字典类型待处理");
                    }
                }
                else
                {
                    var nodefield = fields[i] as DINodeVO;

                    var fieldtype = nodefield.Type.FieldType;
                    var name_lower = nodefield.Name.ToLower();
                    var firstelementtype = nodefield.Type.FirstElementType;
                    var istuple = firstelementtype == DTDefine.TUPLE;

                    if (fieldtype == EFieldType.Single)
                    {
                        if (istuple == false)
                        {
                            sw.WriteLine("\t\t\t\tm_{0}.Deserialize(br);", name_lower);
                        }
                        else
                        {
                            WriteFieldTupleDeserializeByBinary(sw, nodefield, fieldtype);
                        }
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.List)
                    {
                        if (istuple == false)
                        {
                            sw.WriteLine("\t\t\t\tint m_{0}_cnt = br.ReadInt32();", name_lower);
                            sw.WriteLine("\t\t\t\tm_{0} = new List<{1}>();", name_lower, DTDefine.GetTypeByString(firstelementtype));
                            sw.WriteLine("\t\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", name_lower);
                            sw.WriteLine("\t\t\t\t{");
                            sw.WriteLine("\t\t\t\t\tm_{0}.Add(new {1}());", name_lower, DTDefine.GetTypeByString(firstelementtype));
                            sw.WriteLine("\t\t\t\t\tm_{0}[i].Deserialize(br);", name_lower);
                            sw.WriteLine("\t\t\t\t}");
                        }
                        else
                        {
                            WriteFieldTupleDeserializeByBinary(sw, nodefield, fieldtype);
                        }
                        WriteLineFunc(i);
                    }
                    else if (fieldtype == EFieldType.Array)
                    {
                        if (istuple == false)
                        {
                            sw.WriteLine("\t\t\t\tint m_{0}_cnt = br.ReadInt32();", name_lower);
                            sw.WriteLine("\t\t\t\tm_{0} = new {1}[m_{0}_cnt];", name_lower, DTDefine.GetTypeByString(firstelementtype));
                            sw.WriteLine("\t\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", name_lower);
                            sw.WriteLine("\t\t\t\t{");
                            sw.WriteLine("\t\t\t\t\tm_{0}[i] = new {1}();", name_lower, DTDefine.GetTypeByString(firstelementtype));
                            sw.WriteLine("\t\t\t\t\tm_{0}[i].Deserialize(br);", name_lower);
                            sw.WriteLine("\t\t\t\t}");
                        }
                        else
                        {
                            WriteFieldTupleDeserializeByBinary(sw, nodefield, fieldtype);
                        }
                        WriteLineFunc(i);
                    }
                    else
                    {
                        DebugMgr.LogWarning("WriteFieldDeserializeByBinary：字典类型待处理");
                    }
                }
            }

            WriteTryEnd(sw, "false");

            sw.WriteLine("\t\t\treturn true;");
            sw.WriteLine("\t\t}");
        }



        //private static void WriteFieldDeserializeByString(StreamWriter sw, List<AbstractFieldVO> fields)
        //{
        //    sw.WriteLine("\t\tpublic override bool Deserialize(ref string[] fields)");
        //    sw.WriteLine("\t\t{");

        //    WriteTryBegin(sw);
        //    //sw.WriteLine("\t\t\t\tint i = 1;");

        //    int fieldidx = -1;
        //    bool isleaffield = true;
        //    for (int i = 0; i < fields.Count; ++i)
        //    {
        //        if (fields[i] is LeafFieldVO)
        //        {
        //            if (isleaffield == false)
        //            {
        //                sw.WriteLine();
        //                isleaffield = true;
        //            }
        //            fieldidx++;
        //            var leaffieldvo = fields[i] as LeafFieldVO;
        //            var enumvo = ConfigMgr.tableGenericConf.FindEnum(leaffieldvo.Type);
        //            if (enumvo != null)
        //            {
        //                sw.WriteLine("\t\t\t\tm_{0} = ({1})Enum.Parse(typeof({1}), {2} != string.Empty ? {2} : \"Invalid\");", leaffieldvo.Name.ToLower(), enumvo.Name, SupportFieldType.GetStringReaderMethodName("string", string.Format("fields[{0}]", fieldidx)));
        //            }
        //            else
        //            {
        //                //sw.WriteLine("\t\t\t\tm_{0} = {1};", leaffieldvo.Name.ToLower(), SupportFieldType.GetBinaryReaderMethodName(leaffieldvo.Type));
        //                sw.WriteLine("\t\t\t\tm_{0} = {1};", leaffieldvo.Name.ToLower(), SupportFieldType.GetStringReaderMethodName(leaffieldvo.Type, string.Format("fields[{0}]", fieldidx)));
        //            }
        //            //sw.WriteLine("\t\t\t\tm_shelldict.Add(\"{0}\", m_{1});", leaffieldvo.Name, leaffieldvo.Name.ToLower());
        //        }
        //        else
        //        {
        //            if (isleaffield == true)
        //            {
        //                sw.WriteLine();
        //                isleaffield = false;
        //            }

        //            var nodefield = fields[i] as NodeFieldVO;

        //            WriteFieldTupleDeserializeByString(sw, nodefield, ref fieldidx);
        //        }
        //    }
        //    WriteTryEnd(sw, "false");
        //    sw.WriteLine("\t\t\treturn true;");
        //    sw.WriteLine("\t\t}");
        //}

        //private static void WriteFieldTupleDeserializeByString(StreamWriter sw, NodeFieldVO nodefield, ref int fieldidx)
        //{
        //    List<LeafFieldVO> fields = nodefield.FindLeafFields();

        //    string privatefieldname = nodefield.FullName.ToLower().Replace('.', '_');
        //    string publicfieldname = nodefield.FullName.Replace('.', '_');

        //    sw.WriteLine("\t\t\t\tm_{0}.Clear();", privatefieldname);
        //    string typelist = GetTupleType(fields);
        //    //string valuelist = GetTupleConstructValueByString(fields);
        //    string valuelist = string.Empty;

        //    StringBuilder sb = new StringBuilder();

        //    for (int i = 0; i < fields.Count; ++i)
        //    {
        //        var leaffieldvo = fields[i] as LeafFieldVO;
        //        fieldidx++;
        //        sb.Append(string.Format("\t\t\t\t\t{0}", SupportFieldType.GetStringReaderMethodName(leaffieldvo.Type, string.Format("fields[{0}]", fieldidx))));
        //        if (i != fields.Count - 1 || leaffieldvo.ForeightableVO != null)
        //        {
        //            sb.Append(",\n");
        //        }
        //        if (leaffieldvo.ForeightableVO != null)
        //        {
        //            sb.Append(string.Format("\t\t\t\t\tnew {0}Conf()", leaffieldvo.ForeightableVO.Name));
        //            if (i != fields.Count - 1) { sb.Append(",\n"); }
        //        }
        //    }

        //    valuelist = sb.ToString();

        //    sw.WriteLine("\t\t\t\tTuple<{0}> vo = Tuple.Create(\n{1});", typelist, valuelist);
        //    sw.WriteLine("\t\t\t\tm_{0}.Add(vo);", privatefieldname);
        //    //sw.WriteLine("\t\t\t\tm_shelldict.Add(\"{0}\", m_{1});", publicfieldname, privatefieldname);

        //}

        //private static void WriteFieldAppend(StreamWriter sw, TableVO vo)
        //{
        //    var fields = vo.FindAllExportFields();
        //    bool isexist = fields.Exists(c => c is NodeFieldVO);
        //    if (isexist == false)
        //    {
        //        sw.WriteLine("\t\tpublic override bool Append(AbstractItem item)");
        //        sw.WriteLine("\t\t{");
        //        sw.WriteLine("\t\t\treturn true;");
        //        sw.WriteLine("\t\t}");
        //        return;
        //    }
        //    sw.WriteLine("\t\tpublic override bool Append(AbstractItem item)");
        //    sw.WriteLine("\t\t{");
        //    sw.WriteLine(string.Format("\t\t\tvar conf = item as {0}Conf;", vo.TableName));
        //    sw.WriteLine("\t\t\tif (conf == null)");
        //    sw.WriteLine("\t\t\t{");
        //    sw.WriteLine("\t\t\t\treturn false;");
        //    sw.WriteLine("\t\t\t}");
        //    sw.WriteLine();
        //    WriteTryBegin(sw);

        //    for (int i = 0; i < fields.Count; ++i)
        //    {
        //        if (fields[i] is LeafFieldVO)
        //        {
        //            continue;
        //        }
        //        string privatefieldname = fields[i].FullName.ToLower().Replace('.', '_');

        //        sw.WriteLine(string.Format("\t\t\t\tm_{0}.AddRange(conf.m_{0});", privatefieldname));
        //    }
        //    WriteTryEnd(sw, "false");
        //    sw.WriteLine("\t\t\treturn true;");
        //    sw.WriteLine("\t\t}");
        //}

        //private static void WriteConfClone(StreamWriter sw, TableVO vo)
        //{
        //    var fields = vo.FindAllExportFields();

        //    sw.WriteLine("\t\tpublic override bool Clone(AbstractItem item)");
        //    sw.WriteLine("\t\t{");
        //    sw.WriteLine(string.Format("\t\t\tvar conf = item as {0}Conf;", vo.TableName));
        //    sw.WriteLine("\t\t\tif (conf == null)");
        //    sw.WriteLine("\t\t\t{");
        //    sw.WriteLine("\t\t\t\treturn false;");
        //    sw.WriteLine("\t\t\t}");
        //    sw.WriteLine();
        //    WriteTryBegin(sw);

        //    bool isleaffield = true;
        //    for (int i = 0; i < fields.Count; ++i)
        //    {
        //        if (fields[i] is LeafFieldVO)
        //        {
        //            if (isleaffield == false)
        //            {
        //                sw.WriteLine();
        //                isleaffield = true;
        //            }

        //            var leaffieldvo = fields[i] as LeafFieldVO;
        //            sw.WriteLine("\t\t\t\tm_{0} = conf.m_{0};", leaffieldvo.Name.ToLower());
        //        }
        //        else
        //        {
        //            if (isleaffield == true)
        //            {
        //                sw.WriteLine();
        //                isleaffield = false;
        //            }

        //            var nodefield = fields[i] as NodeFieldVO;
        //            string privatefieldname = nodefield.FullName.ToLower().Replace('.', '_');
        //            sw.WriteLine("\t\t\t\tm_{0}.Clear();", privatefieldname);
        //            sw.WriteLine("\t\t\t\tm_{0}.AddRange(conf.m_{0});", privatefieldname);
        //        }
        //    }

        //    WriteTryEnd(sw, "false");
        //    sw.WriteLine("\t\t\treturn true;");
        //    sw.WriteLine("\t\t}");

        //}

        //private static void WriteLinkForeignTable(StreamWriter sw, DITableVO vo)
        //{
        //    var fields = vo.FindChildren();

        //    sw.WriteLine("\t\tpublic override void LinkForeignTable()");
        //    sw.WriteLine("\t\t{");
        //    bool isleaffield = true;
        //    for (int i = 0; i < fields.Count; ++i)
        //    {
        //        if (fields[i] is DIFieldVO)
        //        {
        //            if (isleaffield == false)
        //            {
        //                sw.WriteLine();
        //                isleaffield = true;
        //            }
        //            var difieldvo = fields[i] as DIFieldVO;
        //            if (difieldvo.ForeignTableVO != null)
        //            {
        //                sw.WriteLine("\t\t\tm_{0}conf = TableMgr.Instance.{1}VO.GenericFindVO(m_{2});",
        //                        difieldvo.Name.ToLower(),
        //                        difieldvo.ForeignTableVO.Name,
        //                        difieldvo.Name.ToLower());
        //            }
        //        }
        //        else
        //        {
        //            if (isleaffield == true)
        //            {
        //                sw.WriteLine();
        //                isleaffield = false;
        //            }
        //            var dinodevo = fields[i] as DINodeVO;
        //            var fieldtype = dinodevo.Type.FieldType;
        //            var name_lower = dinodevo.Name.ToLower();
        //            var firstelementtype = dinodevo.Type.FirstElementType;
        //            if (fieldtype == EFieldType.Single)
        //            {
        //                sw.WriteLine("\t\t\tm_{0}.LinkForeignTable();", name_lower);
        //            }
        //            else if (fieldtype == EFieldType.List)
        //            {
        //                sw.WriteLine("\t\t\tint m_{0}_cnt = m_{0}.Count;", name_lower);
        //                sw.WriteLine("\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", name_lower);
        //                sw.WriteLine("\t\t\t{");
        //                sw.WriteLine("\t\t\t\tm_{0}[i].LinkForeignTable();", name_lower, SupportFieldType.GetBinaryReaderMethodName(firstelementtype));
        //                sw.WriteLine("\t\t\t}");
        //            }
        //            else if (fieldtype == EFieldType.Array)
        //            {
        //                sw.WriteLine("\t\t\tint m_{0}_cnt = m_{0}.Length;", name_lower);
        //                sw.WriteLine("\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", name_lower);
        //                sw.WriteLine("\t\t\t{");
        //                sw.WriteLine("\t\t\t\tm_{0}[i].LinkForeignTable();", name_lower, SupportFieldType.GetBinaryReaderMethodName(firstelementtype));
        //                sw.WriteLine("\t\t\t}");
        //            }
        //            else
        //            {
        //                DebugMgr.LogWarning("WriteFieldDeserializeByBinary：字典类型待处理");
        //            }
        //        }
        //        //var difieldvo = fields[i] as DIFieldVO;
        //        //if (difieldvo != null && difieldvo.ForeignTableVO != null)
        //        //{
        //        //    if (difieldvo.Owner is TableVO)
        //        //    {
        //        //        sw.WriteLine("\t\t\tm_{0}conf = TableMgr.Instance.{1}VO.GenericFindVO(m_{2});",
        //        //            difieldvo.Name.ToLower(),
        //        //            difieldvo.ForeignTableVO.Name,
        //        //            difieldvo.Name.ToLower());
        //        //    }
        //        //    else
        //        //    {
        //        //        //var nodefield = difieldvo.Parent;
        //        //        //var nodefullname = nodefield.FullName.ToLower().Replace(",", "_");
        //        //        //var leaffields = nodefield.FindLeafFields();
        //        //        //sw.WriteLine(string.Format("\t\t\tfor (int i = 0; i < m_{0}.Count; ++i)", nodefullname));
        //        //        //sw.WriteLine("\t\t\t{");
        //        //        //int foreighcnt = 0;
        //        //        //for (int j = 0; j < leaffields.Count; ++j)
        //        //        //{
        //        //        //    if (leaffields[j].ForeightableVO != null)
        //        //        //    {
        //        //        //        foreighcnt++;
        //        //        //        sw.WriteLine(string.Format("\t\t\t\tm_{0}[i].Item{1}.Clone(TableMgr.Instance.{2}VO.GenericFindVO(m_{0}[i].Item{3}));",
        //        //        //            nodefullname, j + foreighcnt + 1, leaffields[j].ForeightableVO.Name, j + foreighcnt));
        //        //        //    }
        //        //        //}
        //        //        //sw.WriteLine("\t\t\t}");
        //        //        //sw.WriteLine();
        //        //    }
        //        //}
        //    }

        //    sw.WriteLine("\t\t}");
        //}

        //private static void WriteToString(StreamWriter sw, TableVO vo)
        //{
        //    //var nodefields = vo.FindChildren();

        //    sw.WriteLine("\t\tpublic override string ToString()");
        //    sw.WriteLine("\t\t{");
        //    sw.WriteLine("\t\t\tStringBuilder sb = new StringBuilder();");

        //    //bool isleaffield = true;
        //    //for (int i = 0; i < nodefields.Count; ++i)
        //    //{
        //    //    if (nodefields[i] is LeafFieldVO)
        //    //    {
        //    //        if (isleaffield == false)
        //    //        {
        //    //            sw.WriteLine();
        //    //            isleaffield = true;
        //    //        }

        //    //        var leaffieldvo = nodefields[i] as LeafFieldVO;
        //    //        var enumvo = ConfigMgr.tableGenericConf.FindEnum(leaffieldvo.Type);
        //    //        if (enumvo != null)
        //    //        {
        //    //            sw.WriteLine(string.Format("\t\t\tsb.Append(m_{0}.ToString());", leaffieldvo.Name.ToLower()));
        //    //        }
        //    //        else
        //    //        {
        //    //            sw.WriteLine(string.Format("\t\t\tsb.Append(m_{0});", leaffieldvo.Name.ToLower()));
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //        if (isleaffield == true)
        //    //        {
        //    //            sw.WriteLine();
        //    //            isleaffield = false;
        //    //        }

        //    //        var nodefield = nodefields[i] as NodeFieldVO;
        //    //        string privatefieldname = nodefield.FullName.ToLower().Replace('.', '_');
        //    //        var leaffields = nodefield.FindLeafFields();
        //    //        sw.WriteLine(string.Format("\t\t\tfor (int i = 0; i < m_{0}.Count; ++i)", privatefieldname));
        //    //        sw.WriteLine("\t\t\t{");

        //    //        sw.WriteLine(string.Format("\t\t\t\tvar vo = m_{0}[i];", privatefieldname));

        //    //        sw.WriteLine("\t\t\t\tsb.Append(\"(\");");
        //    //        int n = 0;
        //    //        for (int m = 0; m < leaffields.Count; ++m)
        //    //        {
        //    //            n++;
        //    //            sw.WriteLine(string.Format("\t\t\t\tsb.Append(vo.Item{0});", n));
        //    //            if (m != leaffields.Count - 1)
        //    //            {
        //    //                sw.WriteLine("\t\t\t\tsb.Append(\",\");");
        //    //            }
        //    //            if (leaffields[m].ForeightableVO != null)
        //    //            {
        //    //                n++;
        //    //            }
        //    //        }
        //    //        sw.WriteLine("\t\t\t\tsb.Append(\")\");");

        //    //        sw.WriteLine(string.Format("\t\t\t\tif (i != m_{0}.Count - 1)", privatefieldname));
        //    //        sw.WriteLine("\t\t\t\t{");
        //    //        sw.WriteLine("\t\t\t\t\t sb.Append(\"|\");");
        //    //        sw.WriteLine("\t\t\t\t}");

        //    //        sw.WriteLine("\t\t\t}");

        //    //    }
        //    //}

        //    sw.WriteLine("\t\t\treturn sb.ToString();");
        //    //sw.WriteLine("\t\t\treturn true;");
        //    sw.WriteLine("\t\t}");
        //}

        //private static void WriteColumnMethod(StreamWriter sw, TableVO vo)
        //{
        //    var leaffeilds = vo.FindAllLeafFields();
        //    sw.WriteLine("\t\tprivate readonly string[] m_columnarr = new string[]");
        //    sw.WriteLine("\t\t{");
        //    for (int i = 0; i < leaffeilds.Count; ++i)
        //    {
        //        sw.WriteLine(string.Format("\t\t\t\"{0}\",", leaffeilds[i].FullName));
        //    }
        //    sw.WriteLine("\t\t};");
        //    sw.WriteLine("\t\tpublic override string[] ColumnArr => m_columnarr;");

        //    sw.WriteLine();
        //    sw.WriteLine("\t\tpublic override string GetColumnString()");
        //    sw.WriteLine("\t\t{");
        //    sw.Write("\t\t\treturn \"");

        //    var nodefields = vo.FindAllExportFields();
        //    for (int i = 0; i < nodefields.Count; ++i)
        //    {
        //        if (nodefields[i] is LeafFieldVO)
        //        {
        //            var leaffieldvo = nodefields[i] as LeafFieldVO;
        //            sw.Write(leaffieldvo.Name);
        //        }
        //        else
        //        {
        //            var nodefield = nodefields[i] as NodeFieldVO;
        //            var leaffields = nodefield.FindLeafFields();
        //            sw.Write(nodefield.FullName);
        //            sw.Write("(");
        //            for (int m = 0; m < leaffields.Count; ++m)
        //            {
        //                sw.Write(leaffields[m].Name);
        //                if (m != leaffields.Count - 1)
        //                {
        //                    sw.Write(",");
        //                }
        //            }
        //            sw.Write(")");
        //        }

        //        if (i != nodefields.Count - 1)
        //        {
        //            sw.Write("\\t");
        //        }
        //    }
        //    sw.Write("\";");
        //    sw.WriteLine();

        //    sw.WriteLine("\t\t}");
        //}

        //private static void WriteReferenceLib(StreamWriter sw, TableVO vo)
        //{
        //    var fields = vo.FindAllLeaves();
        //    var unityfieldtypes = new string[] {
        //        DTDefine.VECTOR2,
        //        DTDefine.VECTOR2INT,
        //        DTDefine.VECTOR3,
        //        DTDefine.VECTOR3INT,
        //    };
        //    bool isexist = false;
        //    foreach (var fieldtype in unityfieldtypes)
        //    {
        //        if (fields.Exists(c => c.Type.FirstElementType == fieldtype) == true
        //            || fields.Exists(c => c.Type.SecondElementType == fieldtype) == true)
        //        {
        //            isexist = true;
        //        }
        //    }
        //    if (isexist == true)
        //    {
        //        sw.WriteLine("using UnityEngine;");
        //    }
        //}

        private static void WriteReferenceNamespace(StreamWriter sw, DITableVO vo)
        {
            var fields = vo.FindAllLeaves();
            Action<string> CheckEnumExtern = delegate (string typename)
            {
                if (string.IsNullOrWhiteSpace(typename) == false)
                {
                    var enumvo = ConfigMgr.tableGenericConf.FindEnum(typename);
                    if (enumvo != null && string.IsNullOrWhiteSpace(enumvo.Extern) == false)
                    {
                        sw.WriteLine("using {0};", enumvo.Extern);
                    }
                }
            };
            foreach (var field in fields)
            {
                CheckEnumExtern(field.Type.FirstElementType);
                CheckEnumExtern(field.Type.SecondElementType);
            }
        }



        public static void WritePrimaryKeyTable(StreamWriter sw, DITableVO vo)
        {
            var fields = vo.FindFields();

            string realPrimarykeyType = DTDefine.GetTypeByString(vo.PrimaryKey.Type.Value);
            sw.WriteLine("using System;");
            sw.WriteLine("using System.Collections;");
            sw.WriteLine("using System.Collections.Generic;");
            sw.WriteLine("using System.IO;");
            sw.WriteLine("using System.Text;");
            sw.WriteLine("using UKon.Config;");
            sw.WriteLine("using UKon.Log;");
            sw.WriteLine("using {0};", SysDefine.RootNamespace);
            WriteReferenceNamespace(sw, vo);
            sw.WriteLine();
            sw.WriteLine("namespace UKon.Table");
            sw.WriteLine("{");

            sw.WriteLine("\tpublic class {0}Conf : SingleKeyItem<{1}>", vo.Name, realPrimarykeyType);
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tpublic override {1} PrimaryKey {{ get {{ return m_{0}; }} }}", vo.PrimaryKey.Name.ToLower(), realPrimarykeyType);
            sw.WriteLine();

            WriteFieldDefine(sw, fields);
            sw.WriteLine();

            WriteFieldDeserializeByBinary(sw, fields);
            sw.WriteLine();

            //WriteFieldDeserializeByString(sw, fields);
            //sw.WriteLine();

            //WriteFieldAppend(sw, vo);
            //sw.WriteLine();

            //WriteConfClone(sw, vo);
            //sw.WriteLine();

            //WriteLinkForeignTable(sw, vo);
            //sw.WriteLine();

            //WriteToString(sw, vo);

            sw.WriteLine("\t}");
            sw.WriteLine();

            sw.WriteLine(string.Format("\tpublic class {0} : SingleKeyTable<{0}Conf,{1}>", vo.Name, realPrimarykeyType));
            sw.WriteLine("\t{");
            //sw.WriteLine(string.Format("\t\tprivate readonly string m_name = \"{0}\";", vo.Name));
            sw.Write("\t\tpublic override string Name { get { return \"");
            sw.Write(vo.Name);
            sw.Write("\"; } }");

            sw.WriteLine();
            sw.WriteLine();

            sw.WriteLine("\t\tprivate readonly List<string> m_path = new List<string>()");
            sw.WriteLine("\t\t{");

            var discretevos = ConfigMgr.tableConf.FindDiscreteVOByTableName(vo.Name);
            foreach (var dvo in discretevos)
            {
                string path = dvo.Path.Replace(".csv", "");
                sw.WriteLine(string.Format("\t\t\t@\"{0}\",", path));
            }

            sw.WriteLine("\t\t};");
            sw.WriteLine("\t\tpublic override List<string> Path { get { return m_path; } }");
            sw.WriteLine();

            sw.WriteLine("\t\tpublic override string XmlContent");
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tget");
            sw.WriteLine("\t\t\t{");
            sw.WriteLine("\t\t\t\treturn @\"{0}\";", vo.ToXMLString("\t\t\t\t\t"));
            sw.WriteLine("\t\t\t}");
            sw.WriteLine("\t\t}");
            sw.WriteLine();

            //WriteColumnMethod(sw, vo);
            //sw.WriteLine();

            sw.WriteLine("\t\tpublic override AbstractItem FindVO(string key)");
            sw.WriteLine("\t\t{");
            WriteTryBegin(sw);

            var enumvo = ConfigMgr.tableGenericConf.FindEnum(realPrimarykeyType);
            if (enumvo == null)
            {
                sw.WriteLine(string.Format("\t\t\t\t{0} primarykey = {1};", realPrimarykeyType, SupportFieldType.GetStringReaderMethodName(vo.PrimaryKey.Type.FirstElementType, "key")));
            }
            else
            {
                sw.WriteLine(string.Format("\t\t\t\t{0} primarykey = ({1})Enum.Parse(typeof({1}), {2});", realPrimarykeyType, enumvo.Name, "key"));
            }
            sw.WriteLine("\t\t\t\treturn GenericFindVO(primarykey);");

            WriteTryEnd(sw, "null");
            sw.WriteLine("\t\t}");
            sw.WriteLine();

            sw.WriteLine("\t\tpublic override IEnumerable Range(string min, string max)");
            sw.WriteLine("\t\t{");
            WriteTryBegin(sw);

            if (enumvo == null)
            {
                sw.WriteLine(string.Format("\t\t\t\t{0} range1 = {1};", realPrimarykeyType, SupportFieldType.GetStringReaderMethodName(vo.PrimaryKey.Type.FirstElementType, "min")));
                sw.WriteLine(string.Format("\t\t\t\t{0} range2 = {1};", realPrimarykeyType, SupportFieldType.GetStringReaderMethodName(vo.PrimaryKey.Type.FirstElementType, "max")));
            }
            else
            {
                sw.WriteLine(string.Format("\t\t\t\t{0} range1 = ({1})Enum.Parse(typeof({1}), {2});", realPrimarykeyType, enumvo.Name, "min"));
                sw.WriteLine(string.Format("\t\t\t\t{0} range2 = ({1})Enum.Parse(typeof({1}), {2});", realPrimarykeyType, enumvo.Name, "max"));

            }
            sw.WriteLine("\t\t\t\treturn GenericRange(range1, range2);");

            WriteTryEnd(sw, "null");
            sw.WriteLine("\t\t}");

            sw.WriteLine("\t}");
            sw.WriteLine("}");

        }

        public static void WriteUnionKeyTable(StreamWriter sw, DITableVO vo)
        {
            var fields = vo.FindFields();

            sw.WriteLine("using System;");
            sw.WriteLine("using System.Collections;");
            sw.WriteLine("using System.Collections.Generic;");
            sw.WriteLine("using System.IO;");
            sw.WriteLine("using System.Text;");
            sw.WriteLine("using UKon.Config;");
            sw.WriteLine("using UKon.Log;");
            sw.WriteLine("using {0};", SysDefine.RootNamespace);
            sw.WriteLine();
            sw.WriteLine("namespace UKon.Table");
            sw.WriteLine("{");

            var firstfieldvo = vo.UnionKey.First;
            var secondfieldvo = vo.UnionKey.First;

            sw.WriteLine("\tpublic class {0}Conf : UnionKeyItem<{1},{2}>",
                vo.Name, firstfieldvo.Type.FirstElementType, secondfieldvo.Type.FirstElementType);

            sw.WriteLine("\t{");

            sw.WriteLine("\t\tpublic override {1} UnionKey1 {{ get {{ return m_{0}; }} }}", vo.UnionKey.First.Name.ToLower(), DTDefine.GetTypeByString(firstfieldvo.Type.FirstElementType));
            sw.WriteLine();

            sw.WriteLine("\t\tpublic override {1} UnionKey2 {{ get {{ return m_{0}; }} }}", vo.UnionKey.Second.Name.ToLower(), DTDefine.GetTypeByString(secondfieldvo.Type.FirstElementType));
            sw.WriteLine();

            WriteFieldDefine(sw, fields);
            sw.WriteLine();

            WriteFieldDeserializeByBinary(sw, fields);
            sw.WriteLine();

            //WriteFieldDeserializeByString(sw, fields);
            //sw.WriteLine();

            //WriteFieldAppend(sw, vo);
            //sw.WriteLine();

            //WriteConfClone(sw, vo);
            //sw.WriteLine();

            //WriteLinkForeignTable(sw, vo);
            //sw.WriteLine();

            //WriteToString(sw, vo);

            sw.WriteLine("\t}");

            sw.WriteLine();

            var unionkey1type = vo.UnionKey.First.Type.FirstElementType;
            var unionkey2type = vo.UnionKey.Second.Type.FirstElementType;
            sw.WriteLine("\tpublic class {0} : UnionKeyTable<{0}Conf,{1},{2}>", vo.Name, unionkey1type, unionkey2type);
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tprivate readonly string m_name = \"{0}\";", vo.Name);
            sw.WriteLine("\t\tpublic override string Name { get { return m_name; } }");

            sw.WriteLine("\t\tprivate readonly List<string> m_path = new List<string>()");
            sw.WriteLine("\t\t{");

            var discretevos = ConfigMgr.tableConf.FindDiscreteVOByTableName(vo.Name);
            foreach (var dvo in discretevos)
            {
                string path = dvo.Path.Replace(".csv", "");
                sw.WriteLine(string.Format("\t\t\t@\"{0}\",", path));
            }

            sw.WriteLine("\t\t};");
            sw.WriteLine("\t\tpublic override List<string> Path { get { return m_path; } }");
            sw.WriteLine();

            sw.WriteLine("\t\tpublic override string XmlContent");
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tget");
            sw.WriteLine("\t\t\t{");
            sw.WriteLine("\t\t\t\treturn @\"{0}\";", vo.ToXMLString("\t\t\t\t\t"));
            sw.WriteLine("\t\t\t}");
            sw.WriteLine("\t\t}");
            sw.WriteLine();

            sw.WriteLine("\t\tpublic override AbstractItem FindVO(string key1, string key2)");
            sw.WriteLine("\t\t{");
            WriteTryBegin(sw);

            sw.WriteLine("\t\t\t\t{0} unionkey1 = {1};", DTDefine.GetTypeByString(unionkey1type), SupportFieldType.GetStringReaderMethodName(unionkey1type, "key1"));
            sw.WriteLine("\t\t\t\t{0} unionkey2 = {1};", DTDefine.GetTypeByString(unionkey2type), SupportFieldType.GetStringReaderMethodName(unionkey2type, "key2"));
            sw.WriteLine("\t\t\t\treturn GenericFindVO(unionkey1, unionkey2);");

            WriteTryEnd(sw, "null");
            sw.WriteLine("\t\t}");


            sw.WriteLine("\t\tpublic override IEnumerable Range(string min, string max)");
            sw.WriteLine("\t\t{");
            WriteTryBegin(sw);

            sw.WriteLine("\t\t\t\t{0} range1 = {1};", DTDefine.GetTypeByString(unionkey1type), SupportFieldType.GetStringReaderMethodName(unionkey1type, "min"));
            sw.WriteLine("\t\t\t\t{0} range2 = {1};", DTDefine.GetTypeByString(unionkey2type), SupportFieldType.GetStringReaderMethodName(unionkey2type, "max"));
            sw.WriteLine("\t\t\t\treturn GenericRange(range1, range2);");

            WriteTryEnd(sw, "null");
            sw.WriteLine("\t\t}");

            sw.WriteLine("\t}");
            sw.WriteLine("}");
        }

        public static bool GeneCodeCSharpMgr(List<DITableVO> tables, string path)
        {
            string filepath = System.IO.Path.Combine(path, "TableMgr.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                //sw.WriteLine("Hello World");//开始写入值
                //GeneCSharpUtils.WriteClassBegin(sw, vo);
                //GeneCSharpUtils.WriteClassEnd(sw, vo);
                WriteTableMgr(sw, tables);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        public static bool GeneCodeCSharp(DITableVO vo, string path)
        {
            string srcpath = System.IO.Path.Combine(path, vo.Outputpath);
            srcpath = System.IO.Path.Combine(srcpath, vo.Name);

            FileInfo info = new FileInfo(srcpath);

            if (Directory.Exists(info.DirectoryName) == false)
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            string filepath = System.IO.Path.Combine(info.DirectoryName, string.Format("{0}.cs", vo.Name));
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                switch (vo.TableKeyType)
                {
                    case ETableKeyType.PrimaryKey:
                        WritePrimaryKeyTable(sw, vo);
                        break;
                    case ETableKeyType.UnionKey:
                        WriteUnionKeyTable(sw, vo);
                        break;
                    default:
                        result = false;
                        break;
                }
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }
    }
}
