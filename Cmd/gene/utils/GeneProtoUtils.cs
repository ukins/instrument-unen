﻿using System;
using System.Collections.Generic;
using System.IO;
using UKon.Data;
using UKon.Data.Table;
using UNen.DT;
using UNen.Protocol;
using UNen.Table;

namespace UNen.Cmd
{
    class GeneProtoUtils
    {
        private enum ESwitchLine
        {
            None,
            Single,
            Multi
        }

        private static bool CheckProtoList(List<ProtFieldVO> fields)
        {
            foreach (var field in fields)
            {
                if (field.Type.FieldType == EFieldType.List)
                {
                    return true;
                }
            }
            return false;
        }


        private static void WriteProtoMgr(StreamWriter sw)
        {
            var typeset = ConfigMgr.protoConf.FindAllCustomType();
            sw.WriteLine("using UKon.Proto;");
            sw.WriteLine();
            sw.WriteLine("namespace {0}", SysDefine.RootNamespace);
            sw.WriteLine("{");
            sw.WriteLine("\tpublic partial class ProtoMgr");
            sw.WriteLine("\t{");

            sw.WriteLine("\t\tprivate void InnerInit()");
            sw.WriteLine("\t\t{");
            foreach (string fieldtype in typeset)
            {
                TypeParser parser = new TypeParser(fieldtype);
                //string[] elements = fieldtype.Split(':');
                int cnt = ConfigMgr.protoConf.GetEnumCnt(parser.FirstElementType);
                string firstname = parser.FirstElementType;
                bool isenum = cnt > 0;

                if (parser.FieldType == EFieldType.List)
                {
                    if (isenum == true)
                    {
                        sw.WriteLine(string.Format("\t\t\tm_dict.Add(\"{0}\", new Proto{1}Decoder(EProtoDecoderType.List));", fieldtype, firstname));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\tm_dict.Add(\"{0}\", new CustomProtoVOListDecoder<{1}>());", fieldtype, firstname));
                    }
                }
                else if (parser.FieldType == EFieldType.Array)
                {
                    if (isenum == true)
                    {
                        sw.WriteLine(string.Format("\t\t\tm_dict.Add(\"{0}\", new Proto{1}Decoder(EProtoDecoderType.Array)));", fieldtype, firstname));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\tm_dict.Add(\"{0}\", new CustomProtoVOArrayDecoder<{1}>());", fieldtype, firstname));
                    }
                }
                else if (parser.FieldType == EFieldType.Single)
                {
                    if (isenum == true)
                    {
                        sw.WriteLine(string.Format("\t\t\tm_dict.Add(\"{0}\", new Proto{0}Decoder(EProtoDecoderType.Simple));", firstname));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\tm_dict.Add(\"{0}\", new CustomProtoVODecoder<{0}>());", firstname));
                    }
                }
                //sw.WriteLine();
            }
            sw.WriteLine("\t\t}");

            sw.WriteLine("\t}");
            sw.WriteLine("}");
        }
        public static bool GeneProtoMgr(string path)
        {
            string filepath = Path.Combine(path, "ProtoMgr.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoMgr(sw);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        private static void WriteProtoVO(StreamWriter sw, ProtStructureVO vo)
        {
            var fields = vo.FindAll();
            bool iscontainlist = CheckProtoList(fields);

            sw.WriteLine("using UKon.Network;");
            if (iscontainlist == true)
            {
                sw.WriteLine("using System.Collections.Generic;");
            }
            sw.WriteLine();
            sw.WriteLine("namespace UKon.Proto");
            sw.WriteLine("{");
            string clsstr = vo.IsStruct == true ? "struct" : "class";
            if (vo.IsPartial == true)
            {
                sw.WriteLine(string.Format("\tpublic partial {0} {1} : IAbstractProtoVO", clsstr, vo.Name));
            }
            else
            {
                sw.WriteLine(string.Format("\tpublic {0} {1} : IAbstractProtoVO", clsstr, vo.Name));
            }

            sw.WriteLine("\t{");


            foreach (var field in fields)
            {
                //string[] elements = field.Type.Split(':');
                string firstname = field.Type.FirstElementType;
                string fieldname = field.Name;
                sw.WriteLine("\t\t/// <summary>");
                sw.WriteLine(string.Format("\t\t/// {0}", field.Desc));
                sw.WriteLine("\t\t/// <summary>");
                switch (field.Type.FieldType)
                {
                    case EFieldType.Single:
                        sw.WriteLine(string.Format("\t\tpublic {0} {1};", DTDefine.GetTypeByString(firstname), fieldname));
                        break;
                    case EFieldType.List:
                        sw.WriteLine(string.Format("\t\tpublic List<{0}> {1};", DTDefine.GetTypeByString(firstname), fieldname));
                        break;
                    case EFieldType.Array:
                        sw.WriteLine(string.Format("\t\tpublic {0}[] {1};", DTDefine.GetTypeByString(firstname), fieldname));
                        break;
                }
                sw.WriteLine();
            }

            //ESwitchLine flag = ESwitchLine.None;
            //Encode Method
            sw.WriteLine("\t\tpublic void Encode(ByteStream stream)");
            sw.WriteLine("\t\t{");
            foreach (var field in fields)
            {
                //string[] elements = field.Type.Split(':');
                bool isSysType = SysDefine.CheckType(field.Type.FirstElementType, SysDefine.ProtoTypeConst);
                int enumcnt = ConfigMgr.protoConf.GetEnumCnt(field.Type.FirstElementType);
                if (field.Type.FieldType == EFieldType.Single)
                {
                    if (isSysType == true)
                    {
                        sw.WriteLine(string.Format("\t\t\tstream.Write({0});", field.Name));
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine(string.Format("\t\t\tstream.Write((int){0});", field.Name));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\t{0}.Encode(stream);", field.Name));
                    }
                }
                else if (field.Type.FieldType == EFieldType.List)
                {
                    sw.WriteLine(string.Format("\t\t\tstream.Write({0}.Count);", field.Name));
                    sw.WriteLine(string.Format("\t\t\tfor (int i = 0; i < {0}.Count; i++)", field.Name));
                    sw.WriteLine("\t\t\t{");
                    if (isSysType == true)
                    {
                        sw.WriteLine(string.Format("\t\t\t\tstream.Write({0}[i]);", field.Name));
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine(string.Format("\t\t\t\tstream.Write((int){0}[i]);", field.Name));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i].Encode(stream);", field.Name));
                    }

                    sw.WriteLine("\t\t\t}");
                }
                else if (field.Type.FieldType == EFieldType.Array)
                {
                    sw.WriteLine(string.Format("\t\t\tstream.Write({0}.Length);", field.Name));
                    sw.WriteLine(string.Format("\t\t\tfor (int i = 0; i < {0}.Length; i++)", field.Name));
                    sw.WriteLine("\t\t\t{");
                    //bool isSysType = SysDefine.CheckType(elements[0], SysDefine.ProtoTypeConst);
                    if (isSysType == true)
                    {
                        sw.WriteLine(string.Format("\t\t\t\tstream.Write({0}[i]);", field.Name));
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine(string.Format("\t\t\t\tstream.Write((int){0}[i]);", field.Name));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i].Encode(stream);", field.Name));
                    }

                    sw.WriteLine("\t\t\t}");
                }
            }
            sw.WriteLine("\t\t}");

            sw.WriteLine();
            //flag = ESwitchLine.None;
            //Decode Method
            sw.WriteLine("\t\tpublic void Decode(ByteStream stream)");
            sw.WriteLine("\t\t{");
            foreach (var field in fields)
            {
                string firstname = field.Type.FirstElementType;
                string fieldname = field.Name;
                int enumcnt = ConfigMgr.protoConf.GetEnumCnt(firstname);
                bool isSysType = SysDefine.CheckType(firstname, SysDefine.ProtoTypeConst);

                if (field.Type.FieldType == EFieldType.Single)
                {
                    if (isSysType == true)
                    {
                        sw.WriteLine(string.Format("\t\t\t{0} = {1};", fieldname, SupportFieldType.GetBinaryReaderMethodName(firstname, "stream")));
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine(string.Format("\t\t\t{0} = ({1}){2};", fieldname, firstname, SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "stream")));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\t{0}.Decode(stream);", fieldname));
                    }
                }
                else if (field.Type.FieldType == EFieldType.List)
                {
                    sw.WriteLine("\t\t\tint len = stream.ReadInt32();");
                    sw.WriteLine(string.Format("\t\t\t{0} = new List<{1}>(len); ", fieldname, firstname));
                    sw.WriteLine("\t\t\tfor (int i = 0; i < len; i++)");
                    sw.WriteLine("\t\t\t{");
                    if (isSysType == true)
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i] = {1};", fieldname, SupportFieldType.GetBinaryReaderMethodName(firstname, "stream")));
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i] = ({1}){2};", fieldname, firstname, SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "stream")));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}.Add(new {1}());", fieldname, firstname));
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i].Decode(stream);", fieldname));
                    }

                    sw.WriteLine("\t\t\t}");
                }
                else if (field.Type.FieldType == EFieldType.Array)
                {
                    sw.WriteLine("\t\t\tint len = stream.ReadInt32();");
                    sw.WriteLine(string.Format("\t\t\t{0} = new {1}[len]; ", fieldname, firstname));
                    sw.WriteLine("\t\t\tfor (int i = 0; i < len; i++)");
                    sw.WriteLine("\t\t\t{");
                    if (isSysType == true)
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i] = {1};", fieldname, SupportFieldType.GetBinaryReaderMethodName(firstname, "stream")));
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i] = ({1}){2};", fieldname, firstname, SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "stream")));
                    }
                    else
                    {
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i] = new {1}();", fieldname, firstname));
                        sw.WriteLine(string.Format("\t\t\t\t{0}[i].Decode(stream);", fieldname));
                    }

                    sw.WriteLine("\t\t\t}");
                }
            }
            sw.WriteLine("\t\t}");

            sw.WriteLine("\t}");
            sw.WriteLine("}");
        }
        public static bool GeneProtoVO(ProtStructureVO vo, string path)
        {
            string srcpath = System.IO.Path.Combine(path, vo.Name);

            FileInfo info = new FileInfo(srcpath);

            if (Directory.Exists(info.DirectoryName) == false)
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            string filepath = System.IO.Path.Combine(info.DirectoryName, string.Format("{0}.cs", vo.Name));
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoVO(sw, vo);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        private static void WriteProtoRecvServer(StreamWriter sw, DiscreteVO vo, string disname, string endpointname)
        {
            var tb = vo.GetDataTable();
            var tablevo = vo.TableVO;
            //var exportfields = tablevo.FindAllExportFields();

            //var typeset = ConfigMgr.protoConf.FindAllCustomType();
            sw.WriteLine("using UKon.Proto;");
            sw.WriteLine("using UKon.Network;");
            sw.WriteLine("using System.Collections.Generic;");

            sw.WriteLine();
            sw.WriteLine(string.Format("namespace {0}.{1}", SysDefine.RootNamespace, disname));
            sw.WriteLine("{");
            sw.WriteLine(string.Format("\tpublic class {0}RecvServer : AbstractRecvServer", disname));
            sw.WriteLine("\t{");

            for (int i = 0; i < tb.Rows.Count; ++i)
            {
                GenericDataRow row = tb.Rows[i];
                string proto_name = row["Name"][0].Value;
                string proto_id = row["Id"][0].Value;
                string proto_desc = row["Desc"][0].Value;
                string proto_from = row["From"][0].Value;
                string proto_to = row["To"][0].Value;
                string proto_islua = row["isLua"][0].Value;

                if (proto_to != endpointname)
                {
                    continue;
                }

                var proto_param_names = row["Parameter.Name"];
                var proto_param_types = row["Parameter.Type"];
                var proto_param_descs = row["Parameter.Desc"];
                int len = proto_param_names.Count;

                sw.WriteLine();
                sw.WriteLine("\t\t/// <summary>");
                sw.WriteLine(string.Format("\t\t/// {0}", proto_desc));
                sw.WriteLine("\t\t/// </summary>");
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string desc = proto_param_descs.Count > k ? proto_param_descs[k].Value : "";
                    sw.WriteLine(string.Format("\t\t/// <param name=\"{0}\">{1}</param>", name, desc));
                }
                sw.WriteLine(string.Format("\t\t[CustomProtocolID(protoid: (uint)E{0}Proto.{1})]", disname, CmdUtils.ChangeNameFormatToUnderLineStype(proto_name)));
                sw.Write(string.Format("\t\tpublic bool {0}(", proto_name));
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string type = proto_param_types.Count > k ? proto_param_types[k].Value : "";
                    //string[] elements = type.Split(':');
                    TypeParser parser = new TypeParser(type);

                    if (parser.IsCollection == false)
                    {
                        sw.Write(string.Format("{0} {1}", type, name));
                    }
                    else
                    {
                        string newtype = string.Empty;
                        if (parser.FieldType == EFieldType.List)
                        {
                            newtype = string.Format("List<{0}>", parser.FirstElementType);
                        }
                        else if (parser.FieldType == EFieldType.Array)
                        {
                            newtype = string.Format("{0}[]", parser.FirstElementType);
                        }
                        else if (parser.FieldType == EFieldType.Dict)
                        {
                            newtype = string.Format("Dictionary<{0}, {1}>", parser.FirstElementType, parser.SecondElementType);
                        }
                        sw.Write(string.Format("{0} {1}", newtype, name));
                    }

                    if (k != len - 1)
                    {
                        sw.Write(", ");
                    }
                }
                sw.Write(")");
                sw.WriteLine();
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\treturn true;");
                sw.WriteLine("\t\t}");

            }


            sw.WriteLine("\t}");
            sw.WriteLine("}");

        }
        private static void WriteProtoSendServer(StreamWriter sw, DiscreteVO vo, string disname, string endpointname)
        {
            var tb = vo.GetDataTable();
            var tablevo = vo.TableVO;
            //var exportfields = tablevo.FindAllExportFields();

            var typeset = ConfigMgr.protoConf.FindAllCustomType();
            sw.WriteLine("using UKon.Proto;");
            sw.WriteLine("using UKon.Network;");
            sw.WriteLine("using System.Collections.Generic;");

            sw.WriteLine();
            sw.WriteLine(string.Format("namespace {0}.{1}", SysDefine.RootNamespace, disname));
            sw.WriteLine("{");
            sw.WriteLine(string.Format("\tpublic class {0}SendServer", disname));
            sw.WriteLine("\t{");
            sw.WriteLine(string.Format("\t\tpublic static readonly {0}SendServer Instance = new {0}SendServer();", disname));
            sw.Write(string.Format("\t\tprivate {0}SendServer() ", disname));
            sw.Write("{ ");
            sw.Write("}");
            sw.WriteLine();
            sw.WriteLine();
            sw.WriteLine("\t\tprivate NetMgr m_netMgr = NetMgr.Instance;");

            for (int i = 0; i < tb.Rows.Count; ++i)
            {
                GenericDataRow row = tb.Rows[i];
                string proto_name = row["Name"][0].Value;
                string proto_id = row["Id"][0].Value;
                string proto_desc = row["Desc"][0].Value;
                string proto_from = row["From"][0].Value;
                string proto_to = row["To"][0].Value;
                string proto_islua = row["isLua"][0].Value;

                if (proto_from != endpointname)
                {
                    continue;
                }

                sw.WriteLine();
                var proto_param_names = row["Parameter.Name"];
                var proto_param_types = row["Parameter.Type"];
                var proto_param_descs = row["Parameter.Desc"];
                int len = proto_param_names.Count;

                sw.WriteLine("\t\t/// <summary>");
                sw.WriteLine(string.Format("\t\t/// {0}", proto_desc));
                sw.WriteLine("\t\t/// </summary>");
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string desc = proto_param_descs.Count > k ? proto_param_descs[k].Value : "";
                    sw.WriteLine(string.Format("\t\t/// <param name=\"{0}\">{1}</param>", name, desc));
                }
                //sw.WriteLine(string.Format("\t\t[CustomProtocolID({0})]", proto_id));
                sw.Write(string.Format("\t\tpublic bool {0}(", proto_name));
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string type = proto_param_types.Count > k ? proto_param_types[k].Value : "";
                    //string[] elements = type.Split(':');
                    TypeParser parser = new TypeParser(type);
                    if (string.IsNullOrWhiteSpace(name) == true)
                    {
                        continue;
                    }
                    if (parser.IsCollection == false)
                    {
                        sw.Write(string.Format("{0} {1}", type, name));
                    }
                    else
                    {
                        string newtype = string.Empty;
                        if (parser.FieldType == EFieldType.List)
                        {
                            newtype = string.Format("List<{0}>", parser.FirstElementType);
                        }
                        else if (parser.FieldType == EFieldType.Array)
                        {
                            newtype = string.Format("{0}[]", parser.FirstElementType);
                        }
                        else if (parser.FieldType == EFieldType.Dict)
                        {
                            newtype = string.Format("Dictionary<{0}, {1}>", parser.FirstElementType, parser.SecondElementType);
                        }
                        sw.Write(string.Format("{0} {1}", newtype, name));
                    }

                    if (k != len - 1)
                    {
                        sw.Write(", ");
                    }
                }
                sw.Write(")");
                sw.WriteLine();
                sw.WriteLine("\t\t{");

                sw.WriteLine("\t\t\tNetPacket package = NetPacket.Pop();");
                sw.WriteLine(string.Format("\t\t\tpackage.ProtoId = (uint)E{0}Proto.{1};", disname, CmdUtils.ChangeNameFormatToUnderLineStype(proto_name)));
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string type = proto_param_types.Count > k ? proto_param_types[k].Value : "";
                    string[] elements = type.Split(':');
                    if (string.IsNullOrWhiteSpace(name) == true)
                    {
                        continue;
                    }
                    bool isSysType = SysDefine.CheckType(elements[0], SysDefine.ProtoTypeConst);
                    int cnt = ConfigMgr.protoConf.GetEnumCnt(elements[0]);
                    bool isenum = cnt > 0;
                    if (elements.Length == 1)
                    {
                        if (isSysType == true)
                        {
                            sw.WriteLine(string.Format("\t\t\tpackage.Stream.Write({0});", name));
                        }
                        else if (isenum == true)
                        {
                            sw.WriteLine(string.Format("\t\t\tpackage.Stream.Write((int){0});", name));
                        }
                        else
                        {
                            sw.WriteLine(string.Format("\t\t\t{0}.Encode(package.Stream);", name));
                        }
                    }
                    else
                    {
                        if (elements[1].ToLower() == "list")
                        {
                            sw.WriteLine(string.Format("\t\t\tfor (int i = 0; i < {0}.Count; ++i)", name));
                        }
                        else
                        {
                            sw.WriteLine(string.Format("\t\t\tfor (int i = 0; i < {0}.Length; ++i)", name));
                        }
                        sw.WriteLine("\t\t\t{");
                        if (isSysType == true)
                        {
                            sw.WriteLine(string.Format("\t\t\tpackage.Stream.Write({0}[i]);", name));
                        }
                        else if (isenum == true)
                        {
                            sw.WriteLine(string.Format("\t\t\tpackage.Stream.Write((int){0});", name));
                        }
                        else
                        {
                            sw.WriteLine(string.Format("\t\t\t{0}[i].Encode(package.Stream);", name));
                        }
                        sw.WriteLine("\t\t\t}");
                    }
                }
                sw.WriteLine("\t\t\tm_netMgr.Send(package);");
                sw.WriteLine("\t\t\treturn true;");
                sw.WriteLine("\t\t}");

            }


            sw.WriteLine("\t}");
            sw.WriteLine("}");

        }
        private static bool GeneProtoRecvServer(DITableVO vo, string path, string endpointname)
        {
            var protocoltables = ConfigMgr.tableConf.FindDiscreteVOByTableName(vo.Name);

            bool result = true;
            foreach (var discretevo in protocoltables)
            {
                string disname = CmdUtils.ToUpperFirstChar(discretevo.Owner.Name);

                string modulename = string.Format("{0}Feature", disname);
                string subpath = string.Format("{0}/server/{1}RecvServer.cs", modulename, disname);
                string srcpath = System.IO.Path.Combine(path, subpath);

                FileInfo info = new FileInfo(srcpath);

                if (Directory.Exists(info.DirectoryName) == false)
                {
                    Directory.CreateDirectory(info.DirectoryName);
                }

                FileStream fs1 = null;
                StreamWriter sw = null;
                try
                {
                    fs1 = new FileStream(srcpath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    sw = new StreamWriter(fs1);
                    WriteProtoRecvServer(sw, discretevo, disname, endpointname);
                }
                catch (Exception e)
                {
                    DebugMgr.LogError(e.Message);
                    result = false;
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }
                    if (fs1 != null)
                    {
                        fs1.Close();
                    }
                }
                if (result == false)
                {
                    break;
                }
            }
            return result;
        }
        private static bool GeneProtoSendServer(DITableVO vo, string path, string endpointname)
        {
            var protocoltables = ConfigMgr.tableConf.FindDiscreteVOByTableName(vo.Name);

            bool result = true;
            foreach (var discretevo in protocoltables)
            {
                string disname = CmdUtils.ToUpperFirstChar(discretevo.Owner.Name);

                string modulename = string.Format("{0}Feature", disname);
                string subpath = string.Format("{0}/server/{1}SendServer.cs", modulename, disname);
                string srcpath = System.IO.Path.Combine(path, subpath);

                FileInfo info = new FileInfo(srcpath);

                if (Directory.Exists(info.DirectoryName) == false)
                {
                    Directory.CreateDirectory(info.DirectoryName);
                }

                FileStream fs1 = null;
                StreamWriter sw = null;
                try
                {
                    fs1 = new FileStream(srcpath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    sw = new StreamWriter(fs1);
                    WriteProtoSendServer(sw, discretevo, disname, endpointname);
                }
                catch (Exception e)
                {
                    DebugMgr.LogError(e.Message);
                    result = false;
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }
                    if (fs1 != null)
                    {
                        fs1.Close();
                    }
                }
                if (result == false)
                {
                    break;
                }
            }
            return result;
        }
        public static bool GeneProtoServer(DITableVO vo, string path, string endpointname)
        {
            bool result = false;
            result = GeneProtoRecvServer(vo, path, endpointname);
            if (result == false)
            {
                DebugMgr.LogError("生成ProtoRecv文件失败");
                return false;
            }
            result = GeneProtoSendServer(vo, path, endpointname);
            if (result == false)
            {
                DebugMgr.LogError("生成ProtoSend文件失败");
                return false;
            }

            return result;
        }

        private static void WriteProtoEnum(DITableVO tablevo, StreamWriter sw)
        {
            var protocoltables = ConfigMgr.tableConf.FindDiscreteVOByTableName(tablevo.Name);

            //bool result = true;
            //foreach (var discretevo in protocoltables)
            //{
            var typeset = ConfigMgr.protoConf.FindAllCustomType();
            sw.WriteLine("namespace UKon");
            sw.WriteLine("{");

            foreach (var vo in protocoltables)
            {
                sw.WriteLine("\t/// <summary>");
                sw.WriteLine(string.Format("\t/// {0}协议枚举 Range[{1},{2})", vo.Owner.Name, vo.KeyRangeVO.Min, vo.KeyRangeVO.Max));
                sw.WriteLine("\t/// <summary>");
                string name = CmdUtils.ToUpperFirstChar(vo.Owner.Name);
                sw.WriteLine(string.Format("\tpublic enum E{0}Proto", name));
                sw.WriteLine("\t{");

                var dt = vo.GetDataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var row = dt.Rows[i];
                    var id = row["Id"][0].Value;
                    var protoname = row["Name"][0].Value;
                    sw.WriteLine(string.Format("\t\t{0} = {1},", CmdUtils.ChangeNameFormatToUnderLineStype(protoname), id));

                }

                sw.WriteLine("\t}");
                sw.WriteLine();
            }

            sw.WriteLine("}");
        }
        public static bool GeneProtoEnum(string path)
        {
            var vo = ConfigMgr.tableConf.FindVO("ProtocolTable");
            if (vo == null)
            {
                return false;
            }

            string filepath = Path.Combine(path, "ProtoEnum.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoEnum(vo, sw);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        private static void WriteProtoEnumVO(StreamWriter sw, ProtEnumVO vo)
        {
            var fields = vo.FindAll();

            sw.WriteLine("using UKon.Network;");
            sw.WriteLine("using System;");
            sw.WriteLine("using System.Collections.Generic;");
            sw.WriteLine();
            sw.WriteLine("namespace UKon.Proto");
            sw.WriteLine("{");
            sw.WriteLine(string.Format("\tpublic enum {0}", vo.Name));
            sw.WriteLine("\t{");
            for (int i = 0; i < fields.Count; ++i)
            {
                var field = fields[i];
                sw.WriteLine(string.Format("\t\t{0} = {1},", field.Name, i));

            }
            sw.WriteLine("\t}");
            sw.WriteLine();

            sw.WriteLine(string.Format("\tpublic class Proto{0}Decoder : AbstractProtoDecoder", vo.Name));
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tprivate EProtoDecoderType m_type = EProtoDecoderType.Simple;");
            sw.WriteLine(string.Format("\t\tpublic Proto{0}Decoder(EProtoDecoderType type)", vo.Name));
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tm_type = type;");
            sw.WriteLine("\t\t}");

            sw.WriteLine();
            sw.WriteLine("\t\tpublic override object Decode(ByteStream stream)");
            sw.WriteLine("\t\t{");

            sw.WriteLine("\t\t\tswitch (m_type)");
            sw.WriteLine("\t\t\t{");
            sw.WriteLine("\t\t\t\tcase EProtoDecoderType.Simple:");
            sw.WriteLine("\t\t\t\t\treturn stream.ReadInt32();");
            sw.WriteLine("\t\t\t\tcase EProtoDecoderType.List:");
            sw.WriteLine("\t\t\t\t\t{");
            sw.WriteLine("\t\t\t\t\t\tint len = stream.ReadInt32();");
            sw.WriteLine(string.Format("\t\t\t\t\t\tList<{0}> lst = new List<{0}>(len);", vo.Name));
            sw.WriteLine("\t\t\t\t\t\tfor (int i = 0; i < len; i++)");
            sw.WriteLine("\t\t\t\t\t\t{");
            sw.WriteLine(string.Format("\t\t\t\t\t\t\tlst.Add(({0})stream.ReadInt32());", vo.Name));
            sw.WriteLine("\t\t\t\t\t\t}");
            sw.WriteLine("\t\t\t\t\t\treturn lst;");
            sw.WriteLine("\t\t\t\t\t}");
            sw.WriteLine("\t\t\t\tcase EProtoDecoderType.Array:");
            sw.WriteLine("\t\t\t\t\t{");
            sw.WriteLine("\t\t\t\t\t\tint len = stream.ReadInt32();");
            sw.WriteLine(string.Format("\t\t\t\t\t\t{0}[] arr = new {0}[len];", vo.Name));
            sw.WriteLine("\t\t\t\t\t\tfor (int i = 0; i < len; i++)");
            sw.WriteLine("\t\t\t\t\t\t{");
            sw.WriteLine(string.Format("\t\t\t\t\t\t\tarr[i] = ({0})stream.ReadInt32();", vo.Name));
            sw.WriteLine("\t\t\t\t\t\t}");
            sw.WriteLine("\t\t\t\t\t\treturn arr;");
            sw.WriteLine("\t\t\t\t\t}");
            sw.WriteLine("\t\t\t\tdefault: break;");
            sw.WriteLine("\t\t\t}");
            sw.WriteLine(string.Format("\t\t\tthrow new Exception(\"Proto{0}Decoder .Decode Error!\");", vo.Name));

            sw.WriteLine("\t\t}");
            sw.WriteLine("\t}");
            sw.WriteLine("}");
        }
        public static bool GeneProtoEnumVO(ProtEnumVO vo, string path)
        {
            string srcpath = System.IO.Path.Combine(path, vo.Name);

            FileInfo info = new FileInfo(srcpath);

            if (Directory.Exists(info.DirectoryName) == false)
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            string filepath = System.IO.Path.Combine(info.DirectoryName, string.Format("{0}VO.cs", vo.Name));
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoEnumVO(sw, vo);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

    }
}
