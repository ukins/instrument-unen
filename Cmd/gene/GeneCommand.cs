﻿using UKon.Cmd;

namespace UNen.Cmd
{
    /// <summary>
    /// 配置表结构
    /// </summary>
    class GeneCommand : AbstractCommand
    {
        public static readonly string CmdName = "gene";
        public override string Id => CmdName;

        public GeneCommand()
        {
            m_options.Clear();
            //m_options.Add(new CommonAllOption(this));
            m_options.Add(new GeneFormatOption(this));
            m_options.Add(new GeneTableOption(this));
            m_options.Add(new GeneDirOption(this));
            m_options.Add(new GeneProtocolStructOption(this));
            m_options.Add(new GeneProtocolOption(this));
            //m_options.Add(new GeneTableStructOption(this));
        }


    }
}
