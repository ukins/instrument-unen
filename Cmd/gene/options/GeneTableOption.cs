﻿using System.Collections.Generic;
using System.IO;
using UKon.Cmd;
using UNen.DT;
using UNen.TableGeneric;

namespace UNen.Cmd
{
    class GeneTableOption : AbstractCmdOption
    {
        public override string type => "table";
        private readonly string[] m_name = new string[] { "--table", "-t" };
        public override string[] names => m_name;

        public GeneTableOption(GeneCommand cmd)
            : base(cmd)
        {
        }

        private bool GeneFiles(List<DITableVO> tablevos, string rootDir)
        {
            CmdUtils.ClearDir(rootDir);

            bool result = true;
            //生成Table文件
            string tablepath = Path.Combine(rootDir, "tables");
            foreach (var vo in tablevos)
            {
                result = GeneTableUtils.GeneCodeCSharp(vo, tablepath);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. Table解释文件生成失败! tablename = {0}", vo.Name));
                    return false;
                }
            }

            //生成Struct文件
            string structpath = Path.Combine(rootDir, "structs");
            List<TableStructureVO> protolist = ConfigMgr.tableGenericConf.FindAllStruct();
            foreach (var vo in protolist)
            {
                result = GeneTableStructUtils.GeneTableStruct(vo, structpath);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. TableStruct文件生成失败! structname = {0}", vo.Name));
                    return false;
                }
            }

            //生成TableMgr文件
            result = GeneTableUtils.GeneCodeCSharpMgr(tablevos, rootDir);
            if (result == false)
            {
                DebugMgr.LogWarning("Fail. TableMgr.cs生成失败!");
                return false;
            }

            //生成TableEnum文件
            result = GeneTableStructUtils.GeneTableEnum(rootDir);
            if (result == false)
            {
                DebugMgr.LogWarning("Fail. TableEnum枚举文件生成失败! ");
                return false;
            }
            return true;
        }

        public override bool Process(string[] args)
        {
            bool result = false;

            List<DITableVO> tablevos = ConfigMgr.tableConf.FindAll();

            if (tablevos.Count == 0)
            {
                DebugMgr.LogWarning("Fail. 找不到匹配的表格");
                return true;
            }

            var clientdir = Path.Combine(ConfigMgr.sysconf.GeneCodeClientPath, "Config");
            SysDefine.isClient = true;
            result = GeneFiles(tablevos, clientdir);
            if (result == true)
            {
                DebugMgr.Log("client table code gene succ");
            }
            else
            {
                DebugMgr.Log("client table code gene fail");
                return false;
            }

            var serverdir = Path.Combine(ConfigMgr.sysconf.GeneCodeServerPath, "Config");
            SysDefine.isClient = false;
            result = GeneFiles(tablevos, serverdir);
            if (result == true)
            {
                DebugMgr.Log("server table code gene succ");
            }
            else
            {
                DebugMgr.Log("server table code gene fail");
                return false;
            }

            DebugMgr.WriteLine();
            return true;
        }
    }
}
