﻿using UKon.Cmd;
using System;
using System.IO;

namespace UNen.Cmd
{
    class GeneDirOption : AbstractCmdOption
    {
        public override string type => "dir";
        private readonly string[] m_name = new string[] { "--dir", "-d" };
        public override string[] names => m_name;

        private readonly string TargetDir = string.Empty;

        public GeneDirOption(GeneCommand cmd) : base(cmd)
        {
            TargetDir = ConfigMgr.sysconf.GeneDirPath;
        }


        public override bool Process(string[] args)
        {
            var lst = ConfigMgr.tableConf.FindAllDiscreteVO();

            if (lst.Count == 0)
            {
                DebugMgr.LogWarning("Warning! 没有相关的内容需要创建");
                return true;
            }

            //CmdUtils.ClearDir(TargetDir);

            try
            {
                foreach (var disvo in lst)
                {
                    string srcpath = Path.Combine(TargetDir, disvo.Path);
                    FileInfo info = new FileInfo(srcpath);

                    if (Directory.Exists(info.DirectoryName) == false)
                    {
                        Directory.CreateDirectory(info.DirectoryName);
                    }
                }
            }
            catch (Exception e)
            {
                DebugMgr.LogError(e.ToString());
                return false;
            }

            DebugMgr.Log(string.Format("Succ. 配置表文件夹结构生成成功!"));

            return true;
        }
    }
}
