﻿using UKon.Cmd;
using System.Collections.Generic;
using System.IO;
using UNen.Protocol;

namespace UNen.Cmd
{
    class GeneProtocolStructOption : AbstractCmdOption
    {
        public override string type => "protocol_struct";
        private readonly string[] m_name = new string[] { "--protocol_struct", "-ps" };
        public override string[] names => m_name;

        private static string TargetDir = string.Empty;
        private static string TargetMgrDir = string.Empty;
        public GeneProtocolStructOption(GeneCommand cmd)
            : base(cmd)
        {
            TargetMgrDir = Path.Combine(ConfigMgr.sysconf.GeneCodeClientPath, "Protocol");
            TargetDir = Path.Combine(TargetMgrDir, "generic");
        }

        private bool GeneFiles(string rootDir)
        {
            bool result = true;
            CmdUtils.ClearDir(rootDir);

            string structpath = Path.Combine(rootDir, "structs");
            List<ProtStructureVO> protolist = ConfigMgr.protoConf.FindAllStruct();
            foreach (var vo in protolist)
            {
                result = GeneProtoUtils.GeneProtoVO(vo, structpath);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. C#协议数据结构文件生成失败! structname = {0}", vo.Name));
                    return false;
                }
            }

            string enumpath = Path.Combine(rootDir, "enums");
            List<ProtEnumVO> enumlist = ConfigMgr.protoConf.FindAllEnum();
            foreach (var vo in enumlist)
            {
                result = GeneProtoUtils.GeneProtoEnumVO(vo, enumpath);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. C#协议枚举文件生成失败! enumname = {0}", vo.Name));
                    return false;
                }
            }


            result = GeneProtoUtils.GeneProtoMgr(rootDir);
            if (result == true)
            {
                DebugMgr.Log("Succ. ProtoMgr.cs生成成功!");
            }
            else
            {
                DebugMgr.LogWarning("Fail. ProtoMgr.cs生成失败!");
                return false;
            }

            result = GeneProtoUtils.GeneProtoEnum(rootDir);
            if (result == true)
            {
                DebugMgr.Log("Succ. ProtoEnum.cs生成成功!");
            }
            else
            {
                DebugMgr.LogWarning("Fail. ProtoEnum.cs生成失败!");
                return false;
            }
            return true;
        }

        public override bool Process(string[] args)
        {
            bool result = false;

            var clientdir = Path.Combine(ConfigMgr.sysconf.GeneCodeClientPath, "Protocol");
            SysDefine.isClient = true;
            result = GeneFiles(clientdir);
            if (result == true)
            {
                DebugMgr.Log("client protostruct code gene succ");
            }
            else
            {
                DebugMgr.Log("client protostruct code gene fail");
                return false;
            }

            var serverdir = Path.Combine(ConfigMgr.sysconf.GeneCodeServerPath, "Protocol");
            SysDefine.isClient = false;
            result = GeneFiles(serverdir);
            if (result == true)
            {
                DebugMgr.Log("server protostruct code gene succ");
            }
            else
            {
                DebugMgr.Log("server protostruct code gene fail");
                return false;
            }

            DebugMgr.WriteLine();
            return result;
        }
    }
}
