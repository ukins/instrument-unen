﻿using UKon.Cmd;
using System.IO;

namespace UNen.Cmd
{
    class GeneProtocolOption : AbstractCmdOption
    {
        public override string type => "protocol";
        private readonly string[] m_name = new string[] { "--protocol", "-p" };
        public override string[] names => m_name;

        public override uint ArgumentCnt => 1;

        private static string TargetDir = string.Empty;
        public GeneProtocolOption(GeneCommand cmd)
            : base(cmd)
        {
            TargetDir = ConfigMgr.sysconf.GeneProtoServerPath;
        }

        public override bool Process(string[] args)
        {

            //bool result = false;
            var tablevo = ConfigMgr.tableConf.FindVO("ProtocolTable");

            int argementidx = GetArgumentIdx(args);
            if (args.Length <= argementidx)
            {
                DebugMgr.LogWarning("Warning! 请输入对应参数。");
                DebugMgr.WriteLine();
                return true;
            }
            string endpointname = args[argementidx];
            DebugMgr.Log(endpointname);
            var fieldvo = tablevo.FindLeaves().Find(c => c.Name == "From");
            if (fieldvo == null)
            {
                DebugMgr.LogError("Error! Protocol配置表找不到From字段。");
                DebugMgr.WriteLine();
                return true;
            }
            fieldvo = tablevo.FindLeaves().Find(c => c.Name == "To");
            if (fieldvo == null)
            {
                DebugMgr.LogError("Error! Protocol配置表找不到To字段。");
                DebugMgr.WriteLine();
                return true;
            }
            //bool result = fieldvo.CheckEnumValue(endpointname);
            //if (result == false)
            //{
            //    DebugMgr.LogError("Error! 输入参数未定义。");
            //    DebugMgr.WriteLine();
            //    return true;
            //}


            CmdUtils.ClearDir(TargetDir);
            var protocoltables = ConfigMgr.tableConf.FindDiscreteVOByTableName("ProtocolTable");

            bool result = GeneProtoUtils.GeneProtoServer(tablevo, TargetDir, endpointname);
            if (result == true)
            {
                DebugMgr.Log("Succ");
                DebugMgr.WriteLine();
                return false;
            }
            return result;
        }
    }
}
