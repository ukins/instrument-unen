﻿using System.Collections.Generic;
using System.IO;
using UKon.Cmd;
using UNen.TableGeneric;

namespace UNen.Cmd
{
    class GeneTableStructOption : AbstractCmdOption
    {
        public override string type => "table_struct";
        private readonly string[] m_name = new string[] { "--table_struct", "-ts" };
        public override string[] names => m_name;

        private static string TargetDir = string.Empty;
        private static string TargetMgrDir = string.Empty;
        public GeneTableStructOption(GeneCommand cmd)
            : base(cmd)
        {
            TargetMgrDir = Path.Combine(ConfigMgr.sysconf.GeneCodeClientPath, "Config");
            TargetDir = Path.Combine(TargetMgrDir, "struct");
        }

        public override bool Process(string[] args)
        {
            bool result = false;
            CmdUtils.ClearDir(TargetDir);

            List<TableStructureVO> protolist = ConfigMgr.tableGenericConf.FindAllStruct();
            foreach (var vo in protolist)
            {
                result = GeneTableStructUtils.GeneTableStruct(vo, TargetDir);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. C#自定义配置表数据结构文件生成失败! structname = {0}", vo.Name));
                    return false;
                }
            }

            result = GeneTableStructUtils.GeneTableEnum(TargetMgrDir);
            if (result == true)
            {
                DebugMgr.Log("Succ. TableEnum枚举文件生成成功! ");
            }
            else
            {
                DebugMgr.LogWarning("Fail. TableEnum枚举文件生成失败! ");
                return false;
            }

            return result;
        }
    }
}
