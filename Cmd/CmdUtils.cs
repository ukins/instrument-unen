﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace UNen.Cmd
{
    public class CmdUtils
    {
        /// <summary>
        /// 清空文件夹，若文件夹不存在，则创建一个
        /// </summary>
        /// <param name="path"></param>
        public static void ClearDir(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    DirectoryInfo dir = new DirectoryInfo(path);
                    FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
                    foreach (FileSystemInfo i in fileinfo)
                    {
                        if (i is DirectoryInfo)            //判断是否文件夹
                        {
                            DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                            subdir.Delete(true);          //删除子目录和文件
                        }
                        else
                        {
                            File.Delete(i.FullName);      //删除指定文件
                        }
                    }
                }
                else
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 字符串首字母大写
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToUpperFirstChar(string str)
        {
            char[] a = str.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static string ChangeNameFormatToUnderLineStype(string str)
        {
            string newname = string.Empty;
            Regex reg = new Regex("[A-Z][a-z]+");
            MatchCollection matchs = reg.Matches(str);
            foreach (var match in matchs)
            {
                if (newname == string.Empty)
                {
                    newname = match.ToString().ToUpper();
                }
                else
                {
                    newname = newname + "_" + match.ToString().ToUpper();
                }
            }
            return newname;
        }

    }
}
