﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Cmd;
using UKon.Log;
using UNen.Cmd;

namespace UNen
{
    /// <summary>
    /// 指令管理器
    /// 
    /// 用户输入的指令规则
    /// 
    /// command [options] [params]
    /// 指令名称 选项列表   参数列表
    /// 
    /// 指令，选项，参数均大小写敏感
    /// 指令名称必须放在第一位
    /// 选项列表与参数列表都可以有0个至多个
    /// 选项必须以--开头，不需要使用引号
    /// 参数必须使用引号
    /// 选项列表必须在参数列表前面
    /// 
    /// </summary>
    class CommandMgr
    {
        public static CommandMgr Instance
        {
            get { return Singleton<CommandMgr>.Instance; }
        }

        public CommandMgr()
        {
            Init();
        }

        private Dictionary<string, AbstractCommand> m_dict = new Dictionary<string, AbstractCommand>();

        public void Init()
        {
            m_dict.Clear();
            AddCommand(new ScanCommand());
            AddCommand(new GeneCommand());
            AddCommand(new BuildCommand());
            AddCommand(new PrintCommand());
            AddCommand(new ReloadCommand()); 
            AddCommand(new OpenCommand());
        }

        private void AddCommand(AbstractCommand cmd)
        {
            if (m_dict.ContainsKey(cmd.Id) == false)
            {
                m_dict.Add(cmd.Id, cmd);
            }
            else
            {
                DebugMgr.LogError("Error!,命令重复注册！");
            }
        }

        public void Process(string[] args)
        {
            AbstractCommand cmd;
            bool result = m_dict.TryGetValue(args[0], out cmd);
            if (cmd == null)
            {
                DebugMgr.LogWarning("Warning！ 该指令无法解析");
                return;
            }
            //result = cmd.Construct(args);
            result = cmd.Process(args);
            if(result == false)
            {
                DebugMgr.LogWarning("Warning！ 该指令无法解析");
                return;
            }

        }
    }
}
