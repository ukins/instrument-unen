﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Cmd;

namespace UNen.Cmd
{
    class ReloadAllOption : AbstractCmdOption
    {
        public override string type => "all";
        private readonly string[] m_name = new string[] { "--all", "-a" };
        public override string[] names => m_name;

        private readonly string TargetDir = string.Empty;

        public ReloadAllOption(ReloadCommand cmd) : base(cmd)
        {
        }


        public override bool Process(string[] args)
        {
            ConfigMgr.Instance.Init();
            return true;
        }
    }
}
