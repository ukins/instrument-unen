﻿using System.IO;
using System.Text;
using UKon.Data.Table;
using UNen.DT;

namespace UNen.Cmd
{
    public class BuildUtils
    {    /// <summary>
         /// 生成表格数据相关的二进制文件
         /// </summary>
         /// <param name="vo"></param>
         /// <param name="path"></param>
         /// <returns></returns>
        public static bool BuildFileAlone(DITableVO vo, string path)
        {
            string filepath = Path.Combine(path, string.Format("{0}.bytes", vo.Name.ToLower()));
            FileInfo f = new FileInfo(filepath);
            BinaryWriter bw = new BinaryWriter(f.OpenWrite());
            AbstractDataTable tb = vo.GetDataTable();

            bool result = BuildFileUnite(vo, bw);
            bw.BaseStream.Close();
            bw.Close();
            return result;
        }


        public static bool BuildFileUnite(DITableVO vo, BinaryWriter bw)
        {
            AbstractDataTable tb = vo.GetDataTable();
            tb.Build(bw);
            return true;
        }

        public static string GetHelpDesc()
        {
            StringBuilder sb = new StringBuilder();


            return sb.ToString();
        }
    }
}
