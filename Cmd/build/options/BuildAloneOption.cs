﻿using UKon.Cmd;
using System.Collections.Generic;
using UNen.DT;
using UNen.Table;

namespace UNen.Cmd
{
    /// <summary>
    /// 指令格式
    /// build --alone --all 与 build --alone 同样效果 将所有配置表分别单独打一个二进制文件
    /// build --alone ['ItemTable'] 将指定的二进制文件单独打一个二进制文件
    /// 
    /// </summary>
    class BuildAloneOption : AbstractCmdOption
    {
        public override string type => "alone";
        private readonly string[] m_name = new string[] { "--alone", "-s" };
        public override string[] names => m_name;

        private static string TargetDir = string.Empty;

        protected TableConf m_tableMgr = null;

        public BuildAloneOption(BuildCommand cmd)
            : base(cmd)
        {

            string path = ConfigMgr.sysconf.AppPath;
            //TargetDir = System.IO.Path.Combine(path, "build_alone");
            TargetDir = ConfigMgr.sysconf.BuildAlonePath;
        }

        public override bool Process(string[] args)
        {
            List<DITableVO> tablevos = ConfigMgr.tableConf.FindAll();

            if (tablevos.Count == 0)
            {
                DebugMgr.LogError("Fail. 找不到匹配的表格");
                return true;
            }

            CmdUtils.ClearDir(TargetDir);

            foreach (var vo in tablevos)
            {
                bool result = BuildUtils.BuildFileAlone(vo, TargetDir);
                if (result == true)
                {
                    DebugMgr.Log(string.Format("Succ. 二进制文件生成成功! tablename = {0}", vo.Name));
                }
                else
                {
                    DebugMgr.Log(string.Format("Fail. 二进制文件生成失败! tablename = {0}", vo.Name));
                    break;
                }
            }
            DebugMgr.WriteLine();
            return true;
        }
    }
}
