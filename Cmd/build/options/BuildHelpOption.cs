﻿using UKon.Cmd;

namespace UNen.Cmd
{
    class BuildHelpOption : AbstractCmdOption
    {
        public override string type => "help";
        private readonly string[] m_name = new string[] { "--help", "-h" };
        public override string[] names => m_name;

        public BuildHelpOption(BuildCommand cmd)
            : base(cmd)
        {

        }

        public override bool Process(string[] args)
        {

            return true;
        }
    }
}
