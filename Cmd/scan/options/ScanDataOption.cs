﻿using UKon.Cmd;

namespace UNen.Cmd
{
    class ScanDataOption : AbstractCmdOption
    {
        public override string type => "data";
        private readonly string[] m_name = new string[] { "--data", "-d" };
        public override string[] names => m_name;

        public ScanDataOption(ScanCommand cmd)
            : base(cmd)
        {

        }

        public override bool Process(string[] args)
        {
            bool result = true;
            result = ConfigMgr.tableConf.VerifyStruct();
            if (result == true)
            {
                DebugMgr.Log("Succ. 表格结构验证成功");
            }
            else
            {
                DebugMgr.LogWarning("Fail. 表格结构验证失败\n");
                return false;
            }

            if (result == true)
            {
                result = ConfigMgr.tableConf.VerifySelfData();
            }
            if (result == true)
            {
                result = ConfigMgr.tableConf.VerifyLinkData();
            }
            if (result == true)
            {
                DebugMgr.Log("Succ. 表格数据验证成功\n");
            }
            return result;
        }
    }
}
