﻿using UKon.Cmd;

namespace UNen.Cmd
{
    /// <summary>
    /// 扫描指令
    /// scan --same 'item.csv'
    /// scan --format 'item.csv'
    /// </summary>
    class ScanCommand : AbstractCommand
    {
        public static readonly string CmdName = "scan";
        public override string Id => CmdName;

        public ScanCommand()
        {
            m_options.Clear();
            AddCmdOption(new ScanDataOption(this));
            AddCmdOption(new ScanFormatOption(this));
            AddCmdOption(new ScanHelpOption(this));
        }


    }
}
