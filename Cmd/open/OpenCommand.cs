﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Cmd;

namespace UNen.Cmd
{
    class OpenCommand : AbstractCommand
    {
        public static readonly string CmdName = "open";
        public override string Id => CmdName;

        public OpenCommand()
        {
            m_options.Clear();

            m_options.Add(new OpenSystemPathOption(this));

            //DefaultOption = m_options[0];
        }
    }
}
