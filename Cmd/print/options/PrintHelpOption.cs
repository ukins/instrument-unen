﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Cmd;

namespace UNen.Cmd
{
    public class PrintHelpOption : AbstractCmdOption
    {
        public override string type => "help";
        private readonly string[] m_name = new string[] { "--help", "-h" };
        public override string[] names => m_name;

        public PrintHelpOption(PrintCommand cmd)
           : base(cmd)
        {

        }

        public override bool Process(string[] args)
        {
            DebugMgr.Log("指令格式： print --sysconf");
            DebugMgr.Log("说明：显示系统配置，即UNen.config中的内容经处理之后的内容。");
            DebugMgr.WriteLine();

            DebugMgr.Log("指令格式： print --datatable XXX");
            DebugMgr.Log("说明：显示XXX对应数据表在内存中的信息。一次只能显示一张表的内容。XXX区分大小写。");
            DebugMgr.WriteLine();
            return true;
        }
    }
}
