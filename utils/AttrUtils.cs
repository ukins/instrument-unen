﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using UKon.Data;
using UKon.Data.Table;
using UNen.DT;

namespace UNen
{
    static class AttrUtils
    {
        public static bool GetAttr(XmlAttribute attr, out string str, bool need = false)
        {
            str = string.Empty;
            if (attr != null)
            {
                str = attr.Value;
            }
            else if (need == true)
            {
                return false;
            }
            return true;
        }

        public static bool GetAttr(XmlAttribute attr, out bool b, bool need = false)
        {
            b = false;
            if (attr != null)
            {
                try
                {
                    b = Boolean.Parse(attr.Value);
                }
                catch (FormatException e)
                {
                    DebugMgr.LogWarning(e.Message);
                    return false;
                }
            }
            else if (need == true)
            {
                DebugMgr.LogError("Error. Table Attribute Can Not Be Null");
                return false;
            }
            return true;
        }

        public static bool CheckForeignKeyIsExist(string value, DITableVO foreigntable)
        {
            AbstractDataTable tgtdt = foreigntable.GetDataTable();

            for (int j = 0; j < tgtdt.Rows.Count; ++j)
            {
                GenericDataRow row = tgtdt.Rows[j];
                var leaves = row[foreigntable.PrimaryKey.Name];
                foreach (var item in leaves)
                {
                    foreach (var keyvalue in item.Slice)
                    {
                        if (value == keyvalue)
                        {
                            return true;
                        }
                    }

                }
            }

            return false;
        }


        //public static AbstractFieldVO GetFieldVO(XmlNode node, AbstractFieldVO ower)
        //{
        //    AbstractFieldVO fieldvo;
        //    XmlNodeList childs = node.SelectNodes("field");
        //    if (childs.Count > 0)
        //    {
        //        fieldvo = new NodeFieldVO(ower);
        //    }
        //    else
        //    {
        //        fieldvo = new LeafFieldVO(ower);
        //    }
        //    return fieldvo;
        //}

        #region Merger Table XML
        private static Dictionary<string, XmlNode> m_custom_struct_dict = null;
        private static Dictionary<string, XmlNode> TraversalNodeList(XmlNodeList list)
        {
            Dictionary<string, XmlNode> dict = new Dictionary<string, XmlNode>();

            for (int i = 0; i < list.Count; ++i)
            {
                var node = list[i];
                dict.Add(node.Attributes["name"].Value, node);
            }

            return dict;
        }

        private static void MergeField(XmlNode srcfield, XmlNode tgtfield)
        {
            for (int i = 0; i < tgtfield.Attributes.Count; ++i)
            {
                var attr = tgtfield.Attributes[i];
                //DebugMgr.Log(attr == null ? string.Empty : attr.Name);
                if (srcfield.Attributes[attr.Name] == null)
                {
                    (srcfield as XmlElement).SetAttribute(attr.Name, attr.Value);
                }
                else
                {
                    //(srcfield as XmlElement).SetAttribute(attr.Name, attr.Value);

                    srcfield.Attributes[attr.Name].Value = attr.Value;
                }
            }
        }

        private static void AppendField(XmlNode parentfield, XmlNode childfield, XmlElement element)
        {
            //XmlElement element = tableConf.XmlDoc.CreateElement("field");
            for (int i = 0; i < childfield.Attributes.Count; ++i)
            {
                var attr = childfield.Attributes[i];
                element.SetAttribute(attr.Name, attr.Value);
            }
            (parentfield as XmlElement).AppendChild(element);
        }

        private static bool RecursiveMerge(XmlNode node, XmlDocument tableXML)
        {
            var fieldlist = node.SelectNodes("field");
            for (int j = 0; j < fieldlist.Count; ++j)
            {
                var field = fieldlist[j];
                var attr = field.Attributes["type"];
                if (attr != null)
                {
                    TypeParser customtype = new TypeParser(attr.Value);
                    if (customtype.FirstElementType == DTDefine.TUPLE)
                    {
                        bool result = RecursiveMerge(field, tableXML);
                        if (result == false)
                        {
                            return false;
                        }
                    }
                    else if (SysDefine.CheckType(customtype.FirstElementType, SysDefine.ElementTypeConst) == true)
                    {
                        continue;
                    }
                    //DebugMgr.Log(attr.Value);
                    if (customtype.FieldType == EFieldType.Invalid)
                    {
                        DebugMgr.LogError(string.Format("\t字段类型无法解析 table = {0}, field = {1}, type = {2}",
                            node.Attributes["name"].Value, field.Attributes["name"].Value, attr.Value));
                        return false;
                    }
                    if (customtype.FieldType == EFieldType.Single ||
                        customtype.FieldType == EFieldType.Array ||
                        customtype.FieldType == EFieldType.List)
                    {
                        var elementtype = customtype.FirstElementType;
                        if (m_custom_struct_dict.ContainsKey(elementtype) == true)
                        {
                            var src_fields = TraversalNodeList(field.SelectNodes("field"));
                            var tgt_fields = TraversalNodeList(m_custom_struct_dict[elementtype].SelectNodes("field"));

                            //警告未定义字段
                            foreach (var pair in src_fields)
                            {
                                if (tgt_fields.ContainsKey(pair.Key) == false)
                                {
                                    DebugMgr.LogError(string.Format("\t字段在自定义结构中未声明 table = {0}, field = {1}, type = {2}, attr={3}",
                                        node.Attributes["name"].Value, field.Attributes["name"].Value, attr.Value, pair.Key));
                                    return false;
                                }
                            }

                            //合并
                            foreach (var pair in tgt_fields)
                            {
                                if (src_fields.ContainsKey(pair.Key) == true)
                                {
                                    MergeField(src_fields[pair.Key], pair.Value);
                                }
                                else
                                {
                                    AppendField(field, pair.Value, tableXML.CreateElement("field"));
                                }
                            }
                        }
                    }
                    else if (customtype.FieldType == EFieldType.Dict)
                    {

                    }
                }
            }
            return true;

        }

        public static bool MergeTableXml(XmlDocument tableGenericXML, XmlDocument tableXML)
        {
            var nodelist = tableGenericXML.SelectNodes("/config/structures/structure");
            m_custom_struct_dict = TraversalNodeList(nodelist);

            nodelist = tableXML.SelectNodes("/config/tables/table");
            for (int i = 0; i < nodelist.Count; ++i)
            {
                var node = nodelist[i];
                RecursiveMerge(node, tableXML);
                //var fieldlist = node.SelectNodes("field");
                //for (int j = 0; j < fieldlist.Count; ++j)
                //{
                //    var field = fieldlist[j];
                //    var attr = field.Attributes["type"];
                //    if (attr != null)
                //    {
                //        if (SysDefine.CheckType(attr.Value, SysDefine.ElementTypeConst) == true
                //            && attr.Value != DTDefine.TUPLE)
                //        {
                //            continue;
                //        }
                //        //DebugMgr.Log(attr.Value);
                //        TypeParser customtype = new TypeParser(attr.Value);
                //        if (customtype.FieldType == EFieldType.Invalid)
                //        {
                //            DebugMgr.LogError(string.Format("\t字段类型无法解析 table = {0}, field = {1}, type = {2}",
                //                node.Attributes["name"].Value, field.Attributes["name"].Value, attr.Value));
                //            return false;
                //        }
                //        if (customtype.FieldType == EFieldType.Single ||
                //            customtype.FieldType == EFieldType.Array ||
                //            customtype.FieldType == EFieldType.List)
                //        {
                //            var elementtype = customtype.FirstElementType;
                //            if (custom_struct_dict.ContainsKey(elementtype) == true)
                //            {
                //                var src_fields = TraversalNodeList(field.SelectNodes("field"));
                //                var tgt_fields = TraversalNodeList(custom_struct_dict[elementtype].SelectNodes("field"));

                //                //警告未定义字段
                //                foreach (var pair in src_fields)
                //                {
                //                    if (tgt_fields.ContainsKey(pair.Key) == false)
                //                    {
                //                        DebugMgr.LogError(string.Format("\t字段在自定义结构中未声明 table = {0}, field = {1}, type = {2}, attr={3}",
                //                            node.Attributes["name"].Value, field.Attributes["name"].Value, attr.Value, pair.Key));
                //                        return false;
                //                    }
                //                }

                //                //合并
                //                foreach (var pair in tgt_fields)
                //                {
                //                    if (src_fields.ContainsKey(pair.Key) == true)
                //                    {
                //                        MergeField(src_fields[pair.Key], pair.Value);
                //                    }
                //                    else
                //                    {
                //                        AppendField(field, pair.Value, tableXML.CreateElement("field"));
                //                    }
                //                }
                //            }
                //        }
                //        else if (customtype.FieldType == EFieldType.Dict)
                //        {

                //        }
                //    }
                //}
            }

            return true;
        }
        #endregion

        public static bool CheckNamingSpecification(string name)
        {
            string pattern = @"^[_a-zA-Z]\w*$";
            return Regex.IsMatch(name, pattern);
        }

        public static bool CheckNumber(string number)
        {
            string pattern = @"^[0-9]+$";
            return Regex.IsMatch(number, pattern);
        }

    }
}
