﻿using UKon.Data.Table;
using System;
using System.Text;
using System.Xml;

namespace UNen.Protocol
{
    public class ProtEnumFieldVO : AbstractDataVO, IStructDecorator
    {
        private string m_ownername;
        public ProtEnumFieldVO(string owername)
        {
            m_ownername = owername;
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError(string.Format("Error! 字段名称不能为空，fieldownername={0}", m_ownername));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(m_name) == false)
            {
                DebugMgr.LogError(string.Format("Error。字段名称不规范; fieldname = {0}", m_name));
                return false;
            }

            return true;
        }

        public override void Parse(XmlNode node)
        {
            m_xml_node = node;
            ExtractValue(node, "name", out m_name);
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (m_name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", m_name));
            }
            sb.Append("/>");
            return sb.ToString();
        }
    }
}
