﻿using System.Text;
using System.Xml;
using UKon.Data;
using UKon.Data.Table;

namespace UNen.Protocol
{
    public class ProtFieldVO : AbstractDataVO, IStructDecorator
    {
        /// <summary>
        /// 字段类型
        /// </summary>
        protected string m_type = string.Empty;
        protected TypeParser m_typeparser;
        public TypeParser Type { get { return m_typeparser; } }

        private string m_desc = string.Empty;
        public string Desc => m_desc;

        private string m_ownername;
        public ProtFieldVO(string owername)
        {
            m_ownername = owername;
        }

        protected void ParseField(XmlNode node)
        {
            m_xml_node = node;
            ExtractValue(node, "name", out m_name);
            ExtractValue(node, "type", out m_type);
            ExtractValue(node, "desc", out m_desc);
            //ExtractValue(node, "isvertical", out m_isvertical, true);

            m_typeparser = new TypeParser(m_type);
        }

        public override void Parse(XmlNode node)
        {
            ParseField(node);
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError(string.Format("Error! field结构name属性不能为空，field name={0}", FullName));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(m_name) == false)
            {
                DebugMgr.LogError(string.Format("Error。field结构name属性命名不规范; field name = {0}", FullName));
                return false;
            }

            if (string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError(string.Format("Error! field结构type属性类型不能为空，field name={0}", FullName));
                return false;
            }
            return true;
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (m_name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", m_name));
            }
            if (m_type != string.Empty)
            {
                sb.Append(string.Format("type=\'{0}\' ", m_type));
            }
            if (m_desc != string.Empty)
            {
                sb.Append(string.Format("desc=\'{0}\' ", m_desc));
            }
            sb.Append("/>");
            return sb.ToString();
        }

    }
}
