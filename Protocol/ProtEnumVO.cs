﻿using UKon.Data.Table;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace UNen.Protocol
{
    public class ProtEnumVO : AbstractDataVO, IStructDecorator
    {
        private List<ProtEnumFieldVO> m_fields = new List<ProtEnumFieldVO>();
        public List<ProtEnumFieldVO> FindAll() => m_fields;

        public override void Parse(XmlNode node)
        {
            m_xml_node = node;
            ExtractValue(node, "name", out m_name);

            XmlNodeList fields = node.SelectNodes("field");
            for (int i = 0; i < fields.Count; ++i)
            {
                var field = fields[i];
                ProtEnumFieldVO vo = new ProtEnumFieldVO(m_name);
                vo.Parse(fields[i]);
                m_fields.Add(vo);
            }
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError("Error! 自定义协议枚举类型名称不能为空");
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(m_name) == false)
            {
                DebugMgr.LogError(string.Format("Error。字段名称不规范; fieldname = {0}", m_name));
                return false;
            }

            int cnt = ConfigMgr.protoConf.GetEnumCnt(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error。枚举结构重名; protoenumname = {0}", m_name));
                return false;
            }

            HashSet<string> m_set = new HashSet<string>();
            for (int i = 0; i < m_fields.Count; i++)
            {
                if (m_set.Contains(m_fields[i].Name) == true)
                {
                    DebugMgr.LogError(string.Format("Error。枚举字段重名; protoenumname = {0}, field name = {1}", m_name, m_fields[i].Name));
                    return false;
                }
                m_set.Add(m_fields[i].Name);
            }

            return true;
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (m_name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", m_name));
            }
            sb.AppendLine(">");

            for (int i = 0; i < m_fields.Count; i++)
            {
                sb.Append(m_fields[i].ToXMLString(prefix + "\t"));
                sb.AppendLine();
            }
            sb.Append(prefix);
            sb.Append("</field>");
            return sb.ToString();
        }

    }
}
