﻿using UKon.Data.Table;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace UNen.Protocol
{
    public class ProtStructureVO : AbstractDataVO, IStructDecorator
    {
        private List<ProtFieldVO> m_fields = new List<ProtFieldVO>();
        public List<ProtFieldVO> FindAll() => m_fields;

        private bool m_ispartial = false;
        public bool IsPartial => m_ispartial;

        private bool m_isstruct = false;
        public bool IsStruct => m_isstruct;

        public override void Parse(XmlNode node)
        {
            m_xml_node = node;
            ExtractValue(node, "name", out m_name);
            ExtractValue(node, "ispartial", out m_ispartial);
            ExtractValue(node, "isstruct", out m_isstruct);

            XmlNodeList fields = node.SelectNodes("field");
            for (int i = 0; i < fields.Count; ++i)
            {
                var field = fields[i];
                ProtFieldVO vo = new ProtFieldVO(m_name);
                vo.Parse(fields[i]);
                m_fields.Add(vo);
            }
        }
        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError("Error! 自定义协议类型名称不能为空");
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(m_name) == false)
            {
                DebugMgr.LogError(string.Format("Error。字段名称不规范; fieldname = {0}", m_name));
                return false;
            }

            HashSet<string> m_set = new HashSet<string>();
            for (int i = 0; i < m_fields.Count; i++)
            {
                if (m_set.Contains(m_fields[i].Name) == true)
                {
                    DebugMgr.LogError(string.Format("Error。枚举字段重名; protoenumname = {0}, field name = {1}", m_name, m_fields[i].Name));
                    return false;
                }
                m_set.Add(m_fields[i].Name);
            }

            int cnt = ConfigMgr.protoConf.GetStructCnt(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error。协议结构重名; protostructname = {0}", m_name));
                return false;
            }

            bool result = m_fields.TrueForAll(c => c.CheckStruct());
            return result;
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (m_name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", m_name));
            }
            sb.Append("/>");
            return sb.ToString();
        }
    }
}
