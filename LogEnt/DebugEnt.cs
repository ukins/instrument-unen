﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Log;

namespace UNen.Log
{
    class DebugEnt : ILoggarator
    {
        public void Log(string log)
        {
            Console.WriteLine(log);
        }

        public void Log(string log, params object[] param)
        {
            Console.WriteLine(log);
        }

        public void LogError(string log)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(log);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LogError(string log, params object[] param)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(log);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LogWarning(string log)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(log);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LogWarning(string log, params object[] param)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(log);
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
