﻿using UKon.Log;
using UNen.Log;

namespace UNen
{
    class DebugMgr
    {
        public static void Init()
        {
            LogMgr.Instance.Appoint(new DebugEnt());
        }

        public static void Log(string log)
        {
            LogMgr.Instance.Log(log);
        }


        public static void LogWarning(string log)
        {
            LogMgr.Instance.LogWarning(log);
        }


        public static void LogError(string log)
        {
            LogMgr.Instance.LogError(log);
        }

        public static void WriteLine()
        {
            LogMgr.Instance.Log(string.Empty);
        }

    }
}
