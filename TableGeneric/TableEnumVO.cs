﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace UNen.TableGeneric
{
    public class TableEnumVO
    {
        private string m_name = string.Empty;
        public string Name => m_name;

        private string m_extern = string.Empty;
        public string Extern => m_extern;

        private List<TableEnumFieldVO> m_fields = new List<TableEnumFieldVO>();
        public List<TableEnumFieldVO> FindAll() => m_fields;

        public bool Analyze(XmlNode node)
        {
            bool result = AttrUtils.GetAttr(node.Attributes["name"], out m_name, true);
            if (result == false || string.IsNullOrWhiteSpace(m_name) == true)
            {
                DebugMgr.LogError(string.Format("Error! 协议结构name字段不能为空，protostructname={0}", m_name));
                return false;
            }

            result = AttrUtils.CheckNamingSpecification(m_name);
            if (result == false)
            {
                DebugMgr.LogError(string.Format("Error。协议结构命名不规范; protostructname = {0}", m_name));
                return false;
            }

            AttrUtils.GetAttr(node.Attributes["extern"], out m_extern);

            XmlNodeList fields = node.SelectNodes("field");
            for (int i = 0; i < fields.Count; ++i)
            {
                TableEnumFieldVO vo = new TableEnumFieldVO(m_name);
                result = vo.Analyze(fields[i]);
                if (result == false)
                {
                    return false;
                }
                m_fields.Add(vo);
            }

            return true;
        }


        public bool Verify()
        {
            int cnt = ConfigMgr.tableGenericConf.GetEnumCnt(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error。枚举结构重名; tableenumname = {0}", m_name));
                return false;
            }

            foreach (var field in m_fields)
            {
                bool result = field.Verify();
                if (result == false)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
