﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace unen.TableGeneric
{
    public class TableStructureFieldVO
    {
        private string m_name = string.Empty;
        public string Name => m_name;

        private string m_type = string.Empty;
        public string Type => m_type;

        private string m_desc = string.Empty;
        public string Desc => m_desc;

        public bool isEnum
        {
            get;
            private set;
        }

        private string m_ownername;
        public TableStructureFieldVO(string owername)
        {
            m_ownername = owername;
        }


        public bool Analyze(XmlNode node)
        {
            bool result = AttrUtils.GetAttr(node.Attributes["name"], out m_name, true);
            if (result == false || string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError(string.Format("Error! 字段名称不能为空，fieldownername={0}", m_ownername));
                return false;
            }

            result = AttrUtils.CheckNamingSpecification(m_name);
            if (result == false)
            {
                DebugMgr.LogError(string.Format("Error。字段名称不规范; fieldname = {0}", m_name));
                return false;
            }

            result = AttrUtils.GetAttr(node.Attributes["type"], out m_type, true);
            if (result == false || string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError(string.Format("Error! 字段类型不能为空，fieldname={0}", m_name));
                return false;
            }

            AttrUtils.GetAttr(node.Attributes["desc"], out m_desc);

            return true;
        }

        public bool Verify()
        {
            string[] types = m_type.Split(':');
            if (types.Length < 1 || types.Length > 2)
            {
                DebugMgr.LogError(string.Format("Error! 类型错误。fieldname={0}", m_name));
                return false;
            }
            if (types.Length == 2)
            {
                if ((types[1].ToLower() == "list" || types[1].ToLower() == "array") == false)
                {
                    DebugMgr.LogError(string.Format("Error! 类型错误。fieldname={0}", m_name));
                    DebugMgr.LogError("       仅支持list与array");
                    return false;
                }
            }
            bool isSysType = SysDefine.CheckType(types[0], SysDefine.ProtoTypeConst);
            if (isSysType == false)
            {
                int cnt1 = ConfigMgr.tableGenericConf.GetStructCnt(types[0]);
                int cnt2 = ConfigMgr.tableGenericConf.GetEnumCnt(types[0]);
                if (cnt1 == 0 && cnt2 == 0)
                {
                    DebugMgr.LogError(string.Format("Error! 类型尚未定义。fieldname={0}", m_name));
                    return false;
                }
                if (cnt2 > 0)
                {
                    isEnum = true;
                }
            }

            return true;
        }
    }
}
