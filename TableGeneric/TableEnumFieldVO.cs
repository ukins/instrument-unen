﻿using System.Xml;

namespace UNen.TableGeneric
{
    public class TableEnumFieldVO
    {
        /// <summary>
        /// 枚举字段名称
        /// </summary>
        private string m_name = string.Empty;
        public string Name => m_name;

        private string m_desc = string.Empty;
        public string Desc => m_desc;

        private string m_ownername;
        public TableEnumFieldVO(string owername)
        {
            m_ownername = owername;
        }

        public bool Analyze(XmlNode node)
        {
            bool result = AttrUtils.GetAttr(node.Attributes["name"], out m_name, true);
            if (result == false || string.IsNullOrWhiteSpace(m_name))
            {
                DebugMgr.LogError(string.Format("Error! 字段名称不能为空，fieldownername={0}", m_ownername));
                return false;
            }

            result = AttrUtils.CheckNamingSpecification(m_name);
            if (result == false)
            {
                DebugMgr.LogError(string.Format("Error。字段名称不规范; fieldname = {0}", m_name));
                return false;
            }
            AttrUtils.GetAttr(node.Attributes["desc"], out m_desc);

            return true;
        }

        public bool Verify()
        {
            return true;
        }
    }
}
